[
    {
        "package": {
            "key": "aiofiles",
            "package_name": "aiofiles",
            "installed_version": "23.1.0"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "aiohttp",
            "package_name": "aiohttp",
            "installed_version": "3.8.4"
        },
        "dependencies": [
            {
                "key": "aiosignal",
                "package_name": "aiosignal",
                "installed_version": "1.3.1",
                "required_version": ">=1.1.2"
            },
            {
                "key": "async-timeout",
                "package_name": "async-timeout",
                "installed_version": "4.0.2",
                "required_version": ">=4.0.0a3,<5.0"
            },
            {
                "key": "attrs",
                "package_name": "attrs",
                "installed_version": "23.1.0",
                "required_version": ">=17.3.0"
            },
            {
                "key": "charset-normalizer",
                "package_name": "charset-normalizer",
                "installed_version": "3.1.0",
                "required_version": ">=2.0,<4.0"
            },
            {
                "key": "frozenlist",
                "package_name": "frozenlist",
                "installed_version": "1.3.3",
                "required_version": ">=1.1.1"
            },
            {
                "key": "multidict",
                "package_name": "multidict",
                "installed_version": "6.0.4",
                "required_version": ">=4.5,<7.0"
            },
            {
                "key": "yarl",
                "package_name": "yarl",
                "installed_version": "1.9.2",
                "required_version": ">=1.0,<2.0"
            }
        ]
    },
    {
        "package": {
            "key": "aiosignal",
            "package_name": "aiosignal",
            "installed_version": "1.3.1"
        },
        "dependencies": [
            {
                "key": "frozenlist",
                "package_name": "frozenlist",
                "installed_version": "1.3.3",
                "required_version": ">=1.1.0"
            }
        ]
    },
    {
        "package": {
            "key": "altair",
            "package_name": "altair",
            "installed_version": "5.0.0"
        },
        "dependencies": [
            {
                "key": "jinja2",
                "package_name": "jinja2",
                "installed_version": "3.1.2",
                "required_version": null
            },
            {
                "key": "jsonschema",
                "package_name": "jsonschema",
                "installed_version": "4.17.3",
                "required_version": ">=3.0"
            },
            {
                "key": "numpy",
                "package_name": "numpy",
                "installed_version": "1.24.3",
                "required_version": null
            },
            {
                "key": "pandas",
                "package_name": "pandas",
                "installed_version": "2.0.1",
                "required_version": ">=0.18"
            },
            {
                "key": "toolz",
                "package_name": "toolz",
                "installed_version": "0.12.0",
                "required_version": null
            },
            {
                "key": "typing-extensions",
                "package_name": "typing-extensions",
                "installed_version": "4.5.0",
                "required_version": ">=4.0.1"
            }
        ]
    },
    {
        "package": {
            "key": "anyio",
            "package_name": "anyio",
            "installed_version": "3.6.2"
        },
        "dependencies": [
            {
                "key": "idna",
                "package_name": "idna",
                "installed_version": "3.4",
                "required_version": ">=2.8"
            },
            {
                "key": "sniffio",
                "package_name": "sniffio",
                "installed_version": "1.3.0",
                "required_version": ">=1.1"
            }
        ]
    },
    {
        "package": {
            "key": "argon2-cffi",
            "package_name": "argon2-cffi",
            "installed_version": "21.3.0"
        },
        "dependencies": [
            {
                "key": "argon2-cffi-bindings",
                "package_name": "argon2-cffi-bindings",
                "installed_version": "21.2.0",
                "required_version": null
            }
        ]
    },
    {
        "package": {
            "key": "argon2-cffi-bindings",
            "package_name": "argon2-cffi-bindings",
            "installed_version": "21.2.0"
        },
        "dependencies": [
            {
                "key": "cffi",
                "package_name": "cffi",
                "installed_version": "1.15.1",
                "required_version": ">=1.0.1"
            }
        ]
    },
    {
        "package": {
            "key": "arrow",
            "package_name": "arrow",
            "installed_version": "1.2.3"
        },
        "dependencies": [
            {
                "key": "python-dateutil",
                "package_name": "python-dateutil",
                "installed_version": "2.8.2",
                "required_version": ">=2.7.0"
            }
        ]
    },
    {
        "package": {
            "key": "asttokens",
            "package_name": "asttokens",
            "installed_version": "2.2.1"
        },
        "dependencies": [
            {
                "key": "six",
                "package_name": "six",
                "installed_version": "1.16.0",
                "required_version": null
            }
        ]
    },
    {
        "package": {
            "key": "async-timeout",
            "package_name": "async-timeout",
            "installed_version": "4.0.2"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "attrs",
            "package_name": "attrs",
            "installed_version": "23.1.0"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "backcall",
            "package_name": "backcall",
            "installed_version": "0.2.0"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "beautifulsoup4",
            "package_name": "beautifulsoup4",
            "installed_version": "4.12.2"
        },
        "dependencies": [
            {
                "key": "soupsieve",
                "package_name": "soupsieve",
                "installed_version": "2.4.1",
                "required_version": ">1.2"
            }
        ]
    },
    {
        "package": {
            "key": "bleach",
            "package_name": "bleach",
            "installed_version": "6.0.0"
        },
        "dependencies": [
            {
                "key": "six",
                "package_name": "six",
                "installed_version": "1.16.0",
                "required_version": ">=1.9.0"
            },
            {
                "key": "webencodings",
                "package_name": "webencodings",
                "installed_version": "0.5.1",
                "required_version": null
            }
        ]
    },
    {
        "package": {
            "key": "blis",
            "package_name": "blis",
            "installed_version": "0.7.9"
        },
        "dependencies": [
            {
                "key": "numpy",
                "package_name": "numpy",
                "installed_version": "1.24.3",
                "required_version": ">=1.15.0"
            }
        ]
    },
    {
        "package": {
            "key": "build",
            "package_name": "build",
            "installed_version": "0.10.0"
        },
        "dependencies": [
            {
                "key": "packaging",
                "package_name": "packaging",
                "installed_version": "23.1",
                "required_version": ">=19.0"
            },
            {
                "key": "pyproject-hooks",
                "package_name": "pyproject-hooks",
                "installed_version": "1.0.0",
                "required_version": null
            },
            {
                "key": "tomli",
                "package_name": "tomli",
                "installed_version": "2.0.1",
                "required_version": ">=1.1.0"
            }
        ]
    },
    {
        "package": {
            "key": "cachecontrol",
            "package_name": "CacheControl",
            "installed_version": "0.12.11"
        },
        "dependencies": [
            {
                "key": "msgpack",
                "package_name": "msgpack",
                "installed_version": "1.0.5",
                "required_version": ">=0.5.2"
            },
            {
                "key": "requests",
                "package_name": "requests",
                "installed_version": "2.30.0",
                "required_version": null
            }
        ]
    },
    {
        "package": {
            "key": "cachetools",
            "package_name": "cachetools",
            "installed_version": "5.3.0"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "catalogue",
            "package_name": "catalogue",
            "installed_version": "2.0.8"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "certifi",
            "package_name": "certifi",
            "installed_version": "2023.5.7"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "cffi",
            "package_name": "cffi",
            "installed_version": "1.15.1"
        },
        "dependencies": [
            {
                "key": "pycparser",
                "package_name": "pycparser",
                "installed_version": "2.21",
                "required_version": null
            }
        ]
    },
    {
        "package": {
            "key": "charset-normalizer",
            "package_name": "charset-normalizer",
            "installed_version": "3.1.0"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "cleo",
            "package_name": "cleo",
            "installed_version": "2.0.1"
        },
        "dependencies": [
            {
                "key": "crashtest",
                "package_name": "crashtest",
                "installed_version": "0.4.1",
                "required_version": ">=0.4.1,<0.5.0"
            },
            {
                "key": "rapidfuzz",
                "package_name": "rapidfuzz",
                "installed_version": "2.15.1",
                "required_version": ">=2.2.0,<3.0.0"
            }
        ]
    },
    {
        "package": {
            "key": "click",
            "package_name": "click",
            "installed_version": "8.1.3"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "cmake",
            "package_name": "cmake",
            "installed_version": "3.26.3"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "comm",
            "package_name": "comm",
            "installed_version": "0.1.3"
        },
        "dependencies": [
            {
                "key": "traitlets",
                "package_name": "traitlets",
                "installed_version": "5.9.0",
                "required_version": ">=5.3"
            }
        ]
    },
    {
        "package": {
            "key": "confection",
            "package_name": "confection",
            "installed_version": "0.0.4"
        },
        "dependencies": [
            {
                "key": "pydantic",
                "package_name": "pydantic",
                "installed_version": "1.10.7",
                "required_version": ">=1.7.4,<1.11.0,!=1.8.1,!=1.8"
            },
            {
                "key": "srsly",
                "package_name": "srsly",
                "installed_version": "2.4.6",
                "required_version": ">=2.4.0,<3.0.0"
            }
        ]
    },
    {
        "package": {
            "key": "contourpy",
            "package_name": "contourpy",
            "installed_version": "1.0.7"
        },
        "dependencies": [
            {
                "key": "numpy",
                "package_name": "numpy",
                "installed_version": "1.24.3",
                "required_version": ">=1.16"
            }
        ]
    },
    {
        "package": {
            "key": "crashtest",
            "package_name": "crashtest",
            "installed_version": "0.4.1"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "cryptography",
            "package_name": "cryptography",
            "installed_version": "40.0.2"
        },
        "dependencies": [
            {
                "key": "cffi",
                "package_name": "cffi",
                "installed_version": "1.15.1",
                "required_version": ">=1.12"
            }
        ]
    },
    {
        "package": {
            "key": "cycler",
            "package_name": "cycler",
            "installed_version": "0.11.0"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "cymem",
            "package_name": "cymem",
            "installed_version": "2.0.7"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "db-dtypes",
            "package_name": "db-dtypes",
            "installed_version": "1.1.1"
        },
        "dependencies": [
            {
                "key": "numpy",
                "package_name": "numpy",
                "installed_version": "1.24.3",
                "required_version": ">=1.16.6"
            },
            {
                "key": "packaging",
                "package_name": "packaging",
                "installed_version": "23.1",
                "required_version": ">=17.0"
            },
            {
                "key": "pandas",
                "package_name": "pandas",
                "installed_version": "2.0.1",
                "required_version": ">=0.24.2"
            },
            {
                "key": "pyarrow",
                "package_name": "pyarrow",
                "installed_version": "12.0.0",
                "required_version": ">=3.0.0"
            }
        ]
    },
    {
        "package": {
            "key": "debugpy",
            "package_name": "debugpy",
            "installed_version": "1.6.7"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "decorator",
            "package_name": "decorator",
            "installed_version": "5.1.1"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "defusedxml",
            "package_name": "defusedxml",
            "installed_version": "0.7.1"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "distlib",
            "package_name": "distlib",
            "installed_version": "0.3.6"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "docutils",
            "package_name": "docutils",
            "installed_version": "0.20.1"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "dulwich",
            "package_name": "dulwich",
            "installed_version": "0.20.50"
        },
        "dependencies": [
            {
                "key": "urllib3",
                "package_name": "urllib3",
                "installed_version": "1.26.15",
                "required_version": ">=1.25"
            }
        ]
    },
    {
        "package": {
            "key": "editdistance",
            "package_name": "editdistance",
            "installed_version": "0.6.2"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "exceptiongroup",
            "package_name": "exceptiongroup",
            "installed_version": "1.1.1"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "executing",
            "package_name": "executing",
            "installed_version": "1.2.0"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "fastapi",
            "package_name": "fastapi",
            "installed_version": "0.95.2"
        },
        "dependencies": [
            {
                "key": "pydantic",
                "package_name": "pydantic",
                "installed_version": "1.10.7",
                "required_version": ">=1.6.2,<2.0.0,!=1.8.1,!=1.8,!=1.7.3,!=1.7.2,!=1.7.1,!=1.7"
            },
            {
                "key": "starlette",
                "package_name": "starlette",
                "installed_version": "0.27.0",
                "required_version": ">=0.27.0,<0.28.0"
            }
        ]
    },
    {
        "package": {
            "key": "fastjsonschema",
            "package_name": "fastjsonschema",
            "installed_version": "2.16.3"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "ffmpy",
            "package_name": "ffmpy",
            "installed_version": "0.3.0"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "filelock",
            "package_name": "filelock",
            "installed_version": "3.12.0"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "fonttools",
            "package_name": "fonttools",
            "installed_version": "4.39.4"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "fqdn",
            "package_name": "fqdn",
            "installed_version": "1.5.1"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "frozenlist",
            "package_name": "frozenlist",
            "installed_version": "1.3.3"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "fsspec",
            "package_name": "fsspec",
            "installed_version": "2023.5.0"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "google-api-core",
            "package_name": "google-api-core",
            "installed_version": "2.11.0"
        },
        "dependencies": [
            {
                "key": "google-auth",
                "package_name": "google-auth",
                "installed_version": "2.18.0",
                "required_version": ">=2.14.1,<3.0dev"
            },
            {
                "key": "googleapis-common-protos",
                "package_name": "googleapis-common-protos",
                "installed_version": "1.59.0",
                "required_version": ">=1.56.2,<2.0dev"
            },
            {
                "key": "protobuf",
                "package_name": "protobuf",
                "installed_version": "4.23.0",
                "required_version": ">=3.19.5,<5.0.0dev,!=4.21.5,!=4.21.4,!=4.21.3,!=4.21.2,!=4.21.1,!=4.21.0,!=3.20.1,!=3.20.0"
            },
            {
                "key": "requests",
                "package_name": "requests",
                "installed_version": "2.30.0",
                "required_version": ">=2.18.0,<3.0.0dev"
            }
        ]
    },
    {
        "package": {
            "key": "google-auth",
            "package_name": "google-auth",
            "installed_version": "2.18.0"
        },
        "dependencies": [
            {
                "key": "cachetools",
                "package_name": "cachetools",
                "installed_version": "5.3.0",
                "required_version": ">=2.0.0,<6.0"
            },
            {
                "key": "pyasn1-modules",
                "package_name": "pyasn1-modules",
                "installed_version": "0.3.0",
                "required_version": ">=0.2.1"
            },
            {
                "key": "rsa",
                "package_name": "rsa",
                "installed_version": "4.9",
                "required_version": ">=3.1.4,<5"
            },
            {
                "key": "six",
                "package_name": "six",
                "installed_version": "1.16.0",
                "required_version": ">=1.9.0"
            },
            {
                "key": "urllib3",
                "package_name": "urllib3",
                "installed_version": "1.26.15",
                "required_version": "<2.0"
            }
        ]
    },
    {
        "package": {
            "key": "google-auth-oauthlib",
            "package_name": "google-auth-oauthlib",
            "installed_version": "1.0.0"
        },
        "dependencies": [
            {
                "key": "google-auth",
                "package_name": "google-auth",
                "installed_version": "2.18.0",
                "required_version": ">=2.15.0"
            },
            {
                "key": "requests-oauthlib",
                "package_name": "requests-oauthlib",
                "installed_version": "1.3.1",
                "required_version": ">=0.7.0"
            }
        ]
    },
    {
        "package": {
            "key": "google-cloud-bigquery",
            "package_name": "google-cloud-bigquery",
            "installed_version": "3.10.0"
        },
        "dependencies": [
            {
                "key": "google-api-core",
                "package_name": "google-api-core",
                "installed_version": "2.11.0",
                "required_version": ">=1.31.5,<3.0.0dev,!=2.3.0,!=2.2.*,!=2.1.*,!=2.0.*"
            },
            {
                "key": "google-cloud-core",
                "package_name": "google-cloud-core",
                "installed_version": "2.3.2",
                "required_version": ">=1.6.0,<3.0.0dev"
            },
            {
                "key": "google-resumable-media",
                "package_name": "google-resumable-media",
                "installed_version": "2.5.0",
                "required_version": ">=0.6.0,<3.0dev"
            },
            {
                "key": "grpcio",
                "package_name": "grpcio",
                "installed_version": "1.54.2",
                "required_version": ">=1.47.0,<2.0dev"
            },
            {
                "key": "packaging",
                "package_name": "packaging",
                "installed_version": "23.1",
                "required_version": ">=20.0.0"
            },
            {
                "key": "proto-plus",
                "package_name": "proto-plus",
                "installed_version": "1.22.2",
                "required_version": ">=1.15.0,<2.0.0dev"
            },
            {
                "key": "protobuf",
                "package_name": "protobuf",
                "installed_version": "4.23.0",
                "required_version": ">=3.19.5,<5.0.0dev,!=4.21.5,!=4.21.4,!=4.21.3,!=4.21.2,!=4.21.1,!=4.21.0,!=3.20.1,!=3.20.0"
            },
            {
                "key": "python-dateutil",
                "package_name": "python-dateutil",
                "installed_version": "2.8.2",
                "required_version": ">=2.7.2,<3.0dev"
            },
            {
                "key": "requests",
                "package_name": "requests",
                "installed_version": "2.30.0",
                "required_version": ">=2.21.0,<3.0.0dev"
            }
        ]
    },
    {
        "package": {
            "key": "google-cloud-bigquery-storage",
            "package_name": "google-cloud-bigquery-storage",
            "installed_version": "2.19.1"
        },
        "dependencies": [
            {
                "key": "google-api-core",
                "package_name": "google-api-core",
                "installed_version": "2.11.0",
                "required_version": ">=1.34.0,<3.0.0dev,!=2.9.*,!=2.8.*,!=2.7.*,!=2.6.*,!=2.5.*,!=2.4.*,!=2.3.*,!=2.2.*,!=2.10.*,!=2.1.*,!=2.0.*"
            },
            {
                "key": "proto-plus",
                "package_name": "proto-plus",
                "installed_version": "1.22.2",
                "required_version": ">=1.22.0,<2.0.0dev"
            },
            {
                "key": "protobuf",
                "package_name": "protobuf",
                "installed_version": "4.23.0",
                "required_version": ">=3.19.5,<5.0.0dev,!=4.21.5,!=4.21.4,!=4.21.3,!=4.21.2,!=4.21.1,!=4.21.0,!=3.20.1,!=3.20.0"
            }
        ]
    },
    {
        "package": {
            "key": "google-cloud-core",
            "package_name": "google-cloud-core",
            "installed_version": "2.3.2"
        },
        "dependencies": [
            {
                "key": "google-api-core",
                "package_name": "google-api-core",
                "installed_version": "2.11.0",
                "required_version": ">=1.31.6,<3.0.0dev,!=2.3.0,!=2.2.*,!=2.1.*,!=2.0.*"
            },
            {
                "key": "google-auth",
                "package_name": "google-auth",
                "installed_version": "2.18.0",
                "required_version": ">=1.25.0,<3.0dev"
            }
        ]
    },
    {
        "package": {
            "key": "google-crc32c",
            "package_name": "google-crc32c",
            "installed_version": "1.5.0"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "google-resumable-media",
            "package_name": "google-resumable-media",
            "installed_version": "2.5.0"
        },
        "dependencies": [
            {
                "key": "google-crc32c",
                "package_name": "google-crc32c",
                "installed_version": "1.5.0",
                "required_version": ">=1.0,<2.0dev"
            }
        ]
    },
    {
        "package": {
            "key": "googleapis-common-protos",
            "package_name": "googleapis-common-protos",
            "installed_version": "1.59.0"
        },
        "dependencies": [
            {
                "key": "protobuf",
                "package_name": "protobuf",
                "installed_version": "4.23.0",
                "required_version": ">=3.19.5,<5.0.0dev,!=4.21.5,!=4.21.4,!=4.21.3,!=4.21.2,!=4.21.1,!=3.20.1,!=3.20.0"
            }
        ]
    },
    {
        "package": {
            "key": "gradio",
            "package_name": "gradio",
            "installed_version": "3.31.0"
        },
        "dependencies": [
            {
                "key": "aiofiles",
                "package_name": "aiofiles",
                "installed_version": "23.1.0",
                "required_version": null
            },
            {
                "key": "aiohttp",
                "package_name": "aiohttp",
                "installed_version": "3.8.4",
                "required_version": null
            },
            {
                "key": "altair",
                "package_name": "altair",
                "installed_version": "5.0.0",
                "required_version": ">=4.2.0"
            },
            {
                "key": "fastapi",
                "package_name": "fastapi",
                "installed_version": "0.95.2",
                "required_version": null
            },
            {
                "key": "ffmpy",
                "package_name": "ffmpy",
                "installed_version": "0.3.0",
                "required_version": null
            },
            {
                "key": "gradio-client",
                "package_name": "gradio-client",
                "installed_version": "0.2.5",
                "required_version": ">=0.2.4"
            },
            {
                "key": "httpx",
                "package_name": "httpx",
                "installed_version": "0.24.0",
                "required_version": null
            },
            {
                "key": "huggingface-hub",
                "package_name": "huggingface-hub",
                "installed_version": "0.14.1",
                "required_version": ">=0.13.0"
            },
            {
                "key": "jinja2",
                "package_name": "jinja2",
                "installed_version": "3.1.2",
                "required_version": null
            },
            {
                "key": "markdown-it-py",
                "package_name": "markdown-it-py",
                "installed_version": "2.2.0",
                "required_version": ">=2.0.0"
            },
            {
                "key": "markupsafe",
                "package_name": "markupsafe",
                "installed_version": "2.1.2",
                "required_version": null
            },
            {
                "key": "matplotlib",
                "package_name": "matplotlib",
                "installed_version": "3.7.1",
                "required_version": null
            },
            {
                "key": "mdit-py-plugins",
                "package_name": "mdit-py-plugins",
                "installed_version": "0.3.3",
                "required_version": "<=0.3.3"
            },
            {
                "key": "numpy",
                "package_name": "numpy",
                "installed_version": "1.24.3",
                "required_version": null
            },
            {
                "key": "orjson",
                "package_name": "orjson",
                "installed_version": "3.8.12",
                "required_version": null
            },
            {
                "key": "pandas",
                "package_name": "pandas",
                "installed_version": "2.0.1",
                "required_version": null
            },
            {
                "key": "pillow",
                "package_name": "pillow",
                "installed_version": "9.5.0",
                "required_version": null
            },
            {
                "key": "pydantic",
                "package_name": "pydantic",
                "installed_version": "1.10.7",
                "required_version": null
            },
            {
                "key": "pydub",
                "package_name": "pydub",
                "installed_version": "0.25.1",
                "required_version": null
            },
            {
                "key": "pygments",
                "package_name": "pygments",
                "installed_version": "2.15.1",
                "required_version": ">=2.12.0"
            },
            {
                "key": "python-multipart",
                "package_name": "python-multipart",
                "installed_version": "0.0.6",
                "required_version": null
            },
            {
                "key": "pyyaml",
                "package_name": "pyyaml",
                "installed_version": "6.0",
                "required_version": null
            },
            {
                "key": "requests",
                "package_name": "requests",
                "installed_version": "2.30.0",
                "required_version": null
            },
            {
                "key": "semantic-version",
                "package_name": "semantic-version",
                "installed_version": "2.10.0",
                "required_version": null
            },
            {
                "key": "typing-extensions",
                "package_name": "typing-extensions",
                "installed_version": "4.5.0",
                "required_version": null
            },
            {
                "key": "uvicorn",
                "package_name": "uvicorn",
                "installed_version": "0.22.0",
                "required_version": ">=0.14.0"
            },
            {
                "key": "websockets",
                "package_name": "websockets",
                "installed_version": "11.0.3",
                "required_version": ">=10.0"
            }
        ]
    },
    {
        "package": {
            "key": "gradio-client",
            "package_name": "gradio-client",
            "installed_version": "0.2.5"
        },
        "dependencies": [
            {
                "key": "fsspec",
                "package_name": "fsspec",
                "installed_version": "2023.5.0",
                "required_version": null
            },
            {
                "key": "httpx",
                "package_name": "httpx",
                "installed_version": "0.24.0",
                "required_version": null
            },
            {
                "key": "huggingface-hub",
                "package_name": "huggingface-hub",
                "installed_version": "0.14.1",
                "required_version": ">=0.13.0"
            },
            {
                "key": "packaging",
                "package_name": "packaging",
                "installed_version": "23.1",
                "required_version": null
            },
            {
                "key": "requests",
                "package_name": "requests",
                "installed_version": "2.30.0",
                "required_version": null
            },
            {
                "key": "typing-extensions",
                "package_name": "typing-extensions",
                "installed_version": "4.5.0",
                "required_version": null
            },
            {
                "key": "websockets",
                "package_name": "websockets",
                "installed_version": "11.0.3",
                "required_version": null
            }
        ]
    },
    {
        "package": {
            "key": "graphviz",
            "package_name": "graphviz",
            "installed_version": "0.20.1"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "grpcio",
            "package_name": "grpcio",
            "installed_version": "1.54.2"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "grpcio-status",
            "package_name": "grpcio-status",
            "installed_version": "1.54.2"
        },
        "dependencies": [
            {
                "key": "googleapis-common-protos",
                "package_name": "googleapis-common-protos",
                "installed_version": "1.59.0",
                "required_version": ">=1.5.5"
            },
            {
                "key": "grpcio",
                "package_name": "grpcio",
                "installed_version": "1.54.2",
                "required_version": ">=1.54.2"
            },
            {
                "key": "protobuf",
                "package_name": "protobuf",
                "installed_version": "4.23.0",
                "required_version": ">=4.21.6"
            }
        ]
    },
    {
        "package": {
            "key": "h11",
            "package_name": "h11",
            "installed_version": "0.14.0"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "html5lib",
            "package_name": "html5lib",
            "installed_version": "1.1"
        },
        "dependencies": [
            {
                "key": "six",
                "package_name": "six",
                "installed_version": "1.16.0",
                "required_version": ">=1.9"
            },
            {
                "key": "webencodings",
                "package_name": "webencodings",
                "installed_version": "0.5.1",
                "required_version": null
            }
        ]
    },
    {
        "package": {
            "key": "httpcore",
            "package_name": "httpcore",
            "installed_version": "0.17.0"
        },
        "dependencies": [
            {
                "key": "anyio",
                "package_name": "anyio",
                "installed_version": "3.6.2",
                "required_version": ">=3.0,<5.0"
            },
            {
                "key": "certifi",
                "package_name": "certifi",
                "installed_version": "2023.5.7",
                "required_version": null
            },
            {
                "key": "h11",
                "package_name": "h11",
                "installed_version": "0.14.0",
                "required_version": ">=0.13,<0.15"
            },
            {
                "key": "sniffio",
                "package_name": "sniffio",
                "installed_version": "1.3.0",
                "required_version": "==1.*"
            }
        ]
    },
    {
        "package": {
            "key": "httpx",
            "package_name": "httpx",
            "installed_version": "0.24.0"
        },
        "dependencies": [
            {
                "key": "certifi",
                "package_name": "certifi",
                "installed_version": "2023.5.7",
                "required_version": null
            },
            {
                "key": "httpcore",
                "package_name": "httpcore",
                "installed_version": "0.17.0",
                "required_version": ">=0.15.0,<0.18.0"
            },
            {
                "key": "idna",
                "package_name": "idna",
                "installed_version": "3.4",
                "required_version": null
            },
            {
                "key": "sniffio",
                "package_name": "sniffio",
                "installed_version": "1.3.0",
                "required_version": null
            }
        ]
    },
    {
        "package": {
            "key": "huggingface-hub",
            "package_name": "huggingface-hub",
            "installed_version": "0.14.1"
        },
        "dependencies": [
            {
                "key": "filelock",
                "package_name": "filelock",
                "installed_version": "3.12.0",
                "required_version": null
            },
            {
                "key": "fsspec",
                "package_name": "fsspec",
                "installed_version": "2023.5.0",
                "required_version": null
            },
            {
                "key": "packaging",
                "package_name": "packaging",
                "installed_version": "23.1",
                "required_version": ">=20.9"
            },
            {
                "key": "pyyaml",
                "package_name": "pyyaml",
                "installed_version": "6.0",
                "required_version": ">=5.1"
            },
            {
                "key": "requests",
                "package_name": "requests",
                "installed_version": "2.30.0",
                "required_version": null
            },
            {
                "key": "tqdm",
                "package_name": "tqdm",
                "installed_version": "4.65.0",
                "required_version": ">=4.42.1"
            },
            {
                "key": "typing-extensions",
                "package_name": "typing-extensions",
                "installed_version": "4.5.0",
                "required_version": ">=3.7.4.3"
            }
        ]
    },
    {
        "package": {
            "key": "idna",
            "package_name": "idna",
            "installed_version": "3.4"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "imageio",
            "package_name": "imageio",
            "installed_version": "2.28.1"
        },
        "dependencies": [
            {
                "key": "numpy",
                "package_name": "numpy",
                "installed_version": "1.24.3",
                "required_version": null
            },
            {
                "key": "pillow",
                "package_name": "pillow",
                "installed_version": "9.5.0",
                "required_version": ">=8.3.2"
            }
        ]
    },
    {
        "package": {
            "key": "importlib-metadata",
            "package_name": "importlib-metadata",
            "installed_version": "4.13.0"
        },
        "dependencies": [
            {
                "key": "zipp",
                "package_name": "zipp",
                "installed_version": "3.15.0",
                "required_version": ">=0.5"
            }
        ]
    },
    {
        "package": {
            "key": "importlib-resources",
            "package_name": "importlib-resources",
            "installed_version": "5.12.0"
        },
        "dependencies": [
            {
                "key": "zipp",
                "package_name": "zipp",
                "installed_version": "3.15.0",
                "required_version": ">=3.1.0"
            }
        ]
    },
    {
        "package": {
            "key": "iniconfig",
            "package_name": "iniconfig",
            "installed_version": "2.0.0"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "installer",
            "package_name": "installer",
            "installed_version": "0.7.0"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "ipykernel",
            "package_name": "ipykernel",
            "installed_version": "6.23.1"
        },
        "dependencies": [
            {
                "key": "comm",
                "package_name": "comm",
                "installed_version": "0.1.3",
                "required_version": ">=0.1.1"
            },
            {
                "key": "debugpy",
                "package_name": "debugpy",
                "installed_version": "1.6.7",
                "required_version": ">=1.6.5"
            },
            {
                "key": "ipython",
                "package_name": "ipython",
                "installed_version": "8.13.2",
                "required_version": ">=7.23.1"
            },
            {
                "key": "jupyter-client",
                "package_name": "jupyter-client",
                "installed_version": "8.2.0",
                "required_version": ">=6.1.12"
            },
            {
                "key": "jupyter-core",
                "package_name": "jupyter-core",
                "installed_version": "5.3.0",
                "required_version": ">=4.12,!=5.0.*"
            },
            {
                "key": "matplotlib-inline",
                "package_name": "matplotlib-inline",
                "installed_version": "0.1.6",
                "required_version": ">=0.1"
            },
            {
                "key": "nest-asyncio",
                "package_name": "nest-asyncio",
                "installed_version": "1.5.6",
                "required_version": null
            },
            {
                "key": "packaging",
                "package_name": "packaging",
                "installed_version": "23.1",
                "required_version": null
            },
            {
                "key": "psutil",
                "package_name": "psutil",
                "installed_version": "5.9.5",
                "required_version": null
            },
            {
                "key": "pyzmq",
                "package_name": "pyzmq",
                "installed_version": "25.0.2",
                "required_version": ">=20"
            },
            {
                "key": "tornado",
                "package_name": "tornado",
                "installed_version": "6.3.2",
                "required_version": ">=6.1"
            },
            {
                "key": "traitlets",
                "package_name": "traitlets",
                "installed_version": "5.9.0",
                "required_version": ">=5.4.0"
            }
        ]
    },
    {
        "package": {
            "key": "ipython",
            "package_name": "ipython",
            "installed_version": "8.13.2"
        },
        "dependencies": [
            {
                "key": "backcall",
                "package_name": "backcall",
                "installed_version": "0.2.0",
                "required_version": null
            },
            {
                "key": "decorator",
                "package_name": "decorator",
                "installed_version": "5.1.1",
                "required_version": null
            },
            {
                "key": "jedi",
                "package_name": "jedi",
                "installed_version": "0.18.2",
                "required_version": ">=0.16"
            },
            {
                "key": "matplotlib-inline",
                "package_name": "matplotlib-inline",
                "installed_version": "0.1.6",
                "required_version": null
            },
            {
                "key": "pexpect",
                "package_name": "pexpect",
                "installed_version": "4.8.0",
                "required_version": ">4.3"
            },
            {
                "key": "pickleshare",
                "package_name": "pickleshare",
                "installed_version": "0.7.5",
                "required_version": null
            },
            {
                "key": "prompt-toolkit",
                "package_name": "prompt-toolkit",
                "installed_version": "3.0.38",
                "required_version": ">=3.0.30,<3.1.0,!=3.0.37"
            },
            {
                "key": "pygments",
                "package_name": "pygments",
                "installed_version": "2.15.1",
                "required_version": ">=2.4.0"
            },
            {
                "key": "stack-data",
                "package_name": "stack-data",
                "installed_version": "0.6.2",
                "required_version": null
            },
            {
                "key": "traitlets",
                "package_name": "traitlets",
                "installed_version": "5.9.0",
                "required_version": ">=5"
            },
            {
                "key": "typing-extensions",
                "package_name": "typing-extensions",
                "installed_version": "4.5.0",
                "required_version": null
            }
        ]
    },
    {
        "package": {
            "key": "ipython-genutils",
            "package_name": "ipython-genutils",
            "installed_version": "0.2.0"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "ipywidgets",
            "package_name": "ipywidgets",
            "installed_version": "8.0.6"
        },
        "dependencies": [
            {
                "key": "ipykernel",
                "package_name": "ipykernel",
                "installed_version": "6.23.1",
                "required_version": ">=4.5.1"
            },
            {
                "key": "ipython",
                "package_name": "ipython",
                "installed_version": "8.13.2",
                "required_version": ">=6.1.0"
            },
            {
                "key": "jupyterlab-widgets",
                "package_name": "jupyterlab-widgets",
                "installed_version": "3.0.7",
                "required_version": "~=3.0.7"
            },
            {
                "key": "traitlets",
                "package_name": "traitlets",
                "installed_version": "5.9.0",
                "required_version": ">=4.3.1"
            },
            {
                "key": "widgetsnbextension",
                "package_name": "widgetsnbextension",
                "installed_version": "4.0.7",
                "required_version": "~=4.0.7"
            }
        ]
    },
    {
        "package": {
            "key": "isoduration",
            "package_name": "isoduration",
            "installed_version": "20.11.0"
        },
        "dependencies": [
            {
                "key": "arrow",
                "package_name": "arrow",
                "installed_version": "1.2.3",
                "required_version": ">=0.15.0"
            }
        ]
    },
    {
        "package": {
            "key": "jaraco.classes",
            "package_name": "jaraco.classes",
            "installed_version": "3.2.3"
        },
        "dependencies": [
            {
                "key": "more-itertools",
                "package_name": "more-itertools",
                "installed_version": "9.1.0",
                "required_version": null
            }
        ]
    },
    {
        "package": {
            "key": "jedi",
            "package_name": "jedi",
            "installed_version": "0.18.2"
        },
        "dependencies": [
            {
                "key": "parso",
                "package_name": "parso",
                "installed_version": "0.8.3",
                "required_version": ">=0.8.0,<0.9.0"
            }
        ]
    },
    {
        "package": {
            "key": "jeepney",
            "package_name": "jeepney",
            "installed_version": "0.8.0"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "jinja2",
            "package_name": "Jinja2",
            "installed_version": "3.1.2"
        },
        "dependencies": [
            {
                "key": "markupsafe",
                "package_name": "MarkupSafe",
                "installed_version": "2.1.2",
                "required_version": ">=2.0"
            }
        ]
    },
    {
        "package": {
            "key": "joblib",
            "package_name": "joblib",
            "installed_version": "1.2.0"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "jsonpointer",
            "package_name": "jsonpointer",
            "installed_version": "2.3"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "jsonschema",
            "package_name": "jsonschema",
            "installed_version": "4.17.3"
        },
        "dependencies": [
            {
                "key": "attrs",
                "package_name": "attrs",
                "installed_version": "23.1.0",
                "required_version": ">=17.4.0"
            },
            {
                "key": "pyrsistent",
                "package_name": "pyrsistent",
                "installed_version": "0.19.3",
                "required_version": ">=0.14.0,!=0.17.2,!=0.17.1,!=0.17.0"
            }
        ]
    },
    {
        "package": {
            "key": "jupyter",
            "package_name": "jupyter",
            "installed_version": "1.0.0"
        },
        "dependencies": [
            {
                "key": "ipykernel",
                "package_name": "ipykernel",
                "installed_version": "6.23.1",
                "required_version": null
            },
            {
                "key": "ipywidgets",
                "package_name": "ipywidgets",
                "installed_version": "8.0.6",
                "required_version": null
            },
            {
                "key": "jupyter-console",
                "package_name": "jupyter-console",
                "installed_version": "6.6.3",
                "required_version": null
            },
            {
                "key": "nbconvert",
                "package_name": "nbconvert",
                "installed_version": "7.4.0",
                "required_version": null
            },
            {
                "key": "notebook",
                "package_name": "notebook",
                "installed_version": "6.5.4",
                "required_version": null
            },
            {
                "key": "qtconsole",
                "package_name": "qtconsole",
                "installed_version": "5.4.3",
                "required_version": null
            }
        ]
    },
    {
        "package": {
            "key": "jupyter-client",
            "package_name": "jupyter-client",
            "installed_version": "8.2.0"
        },
        "dependencies": [
            {
                "key": "importlib-metadata",
                "package_name": "importlib-metadata",
                "installed_version": "4.13.0",
                "required_version": ">=4.8.3"
            },
            {
                "key": "jupyter-core",
                "package_name": "jupyter-core",
                "installed_version": "5.3.0",
                "required_version": ">=4.12,!=5.0.*"
            },
            {
                "key": "python-dateutil",
                "package_name": "python-dateutil",
                "installed_version": "2.8.2",
                "required_version": ">=2.8.2"
            },
            {
                "key": "pyzmq",
                "package_name": "pyzmq",
                "installed_version": "25.0.2",
                "required_version": ">=23.0"
            },
            {
                "key": "tornado",
                "package_name": "tornado",
                "installed_version": "6.3.2",
                "required_version": ">=6.2"
            },
            {
                "key": "traitlets",
                "package_name": "traitlets",
                "installed_version": "5.9.0",
                "required_version": ">=5.3"
            }
        ]
    },
    {
        "package": {
            "key": "jupyter-console",
            "package_name": "jupyter-console",
            "installed_version": "6.6.3"
        },
        "dependencies": [
            {
                "key": "ipykernel",
                "package_name": "ipykernel",
                "installed_version": "6.23.1",
                "required_version": ">=6.14"
            },
            {
                "key": "ipython",
                "package_name": "ipython",
                "installed_version": "8.13.2",
                "required_version": null
            },
            {
                "key": "jupyter-client",
                "package_name": "jupyter-client",
                "installed_version": "8.2.0",
                "required_version": ">=7.0.0"
            },
            {
                "key": "jupyter-core",
                "package_name": "jupyter-core",
                "installed_version": "5.3.0",
                "required_version": ">=4.12,!=5.0.*"
            },
            {
                "key": "prompt-toolkit",
                "package_name": "prompt-toolkit",
                "installed_version": "3.0.38",
                "required_version": ">=3.0.30"
            },
            {
                "key": "pygments",
                "package_name": "pygments",
                "installed_version": "2.15.1",
                "required_version": null
            },
            {
                "key": "pyzmq",
                "package_name": "pyzmq",
                "installed_version": "25.0.2",
                "required_version": ">=17"
            },
            {
                "key": "traitlets",
                "package_name": "traitlets",
                "installed_version": "5.9.0",
                "required_version": ">=5.4"
            }
        ]
    },
    {
        "package": {
            "key": "jupyter-core",
            "package_name": "jupyter-core",
            "installed_version": "5.3.0"
        },
        "dependencies": [
            {
                "key": "platformdirs",
                "package_name": "platformdirs",
                "installed_version": "2.6.2",
                "required_version": ">=2.5"
            },
            {
                "key": "traitlets",
                "package_name": "traitlets",
                "installed_version": "5.9.0",
                "required_version": ">=5.3"
            }
        ]
    },
    {
        "package": {
            "key": "jupyter-events",
            "package_name": "jupyter-events",
            "installed_version": "0.6.3"
        },
        "dependencies": [
            {
                "key": "jsonschema",
                "package_name": "jsonschema",
                "installed_version": "4.17.3",
                "required_version": ">=3.2.0"
            },
            {
                "key": "python-json-logger",
                "package_name": "python-json-logger",
                "installed_version": "2.0.7",
                "required_version": ">=2.0.4"
            },
            {
                "key": "pyyaml",
                "package_name": "pyyaml",
                "installed_version": "6.0",
                "required_version": ">=5.3"
            },
            {
                "key": "rfc3339-validator",
                "package_name": "rfc3339-validator",
                "installed_version": "0.1.4",
                "required_version": null
            },
            {
                "key": "rfc3986-validator",
                "package_name": "rfc3986-validator",
                "installed_version": "0.1.1",
                "required_version": ">=0.1.1"
            },
            {
                "key": "traitlets",
                "package_name": "traitlets",
                "installed_version": "5.9.0",
                "required_version": ">=5.3"
            }
        ]
    },
    {
        "package": {
            "key": "jupyter-server",
            "package_name": "jupyter-server",
            "installed_version": "2.5.0"
        },
        "dependencies": [
            {
                "key": "anyio",
                "package_name": "anyio",
                "installed_version": "3.6.2",
                "required_version": ">=3.1.0"
            },
            {
                "key": "argon2-cffi",
                "package_name": "argon2-cffi",
                "installed_version": "21.3.0",
                "required_version": null
            },
            {
                "key": "jinja2",
                "package_name": "jinja2",
                "installed_version": "3.1.2",
                "required_version": null
            },
            {
                "key": "jupyter-client",
                "package_name": "jupyter-client",
                "installed_version": "8.2.0",
                "required_version": ">=7.4.4"
            },
            {
                "key": "jupyter-core",
                "package_name": "jupyter-core",
                "installed_version": "5.3.0",
                "required_version": ">=4.12,!=5.0.*"
            },
            {
                "key": "jupyter-events",
                "package_name": "jupyter-events",
                "installed_version": "0.6.3",
                "required_version": ">=0.4.0"
            },
            {
                "key": "jupyter-server-terminals",
                "package_name": "jupyter-server-terminals",
                "installed_version": "0.4.4",
                "required_version": null
            },
            {
                "key": "nbconvert",
                "package_name": "nbconvert",
                "installed_version": "7.4.0",
                "required_version": ">=6.4.4"
            },
            {
                "key": "nbformat",
                "package_name": "nbformat",
                "installed_version": "5.8.0",
                "required_version": ">=5.3.0"
            },
            {
                "key": "packaging",
                "package_name": "packaging",
                "installed_version": "23.1",
                "required_version": null
            },
            {
                "key": "prometheus-client",
                "package_name": "prometheus-client",
                "installed_version": "0.16.0",
                "required_version": null
            },
            {
                "key": "pyzmq",
                "package_name": "pyzmq",
                "installed_version": "25.0.2",
                "required_version": ">=24"
            },
            {
                "key": "send2trash",
                "package_name": "send2trash",
                "installed_version": "1.8.2",
                "required_version": null
            },
            {
                "key": "terminado",
                "package_name": "terminado",
                "installed_version": "0.17.1",
                "required_version": ">=0.8.3"
            },
            {
                "key": "tornado",
                "package_name": "tornado",
                "installed_version": "6.3.2",
                "required_version": ">=6.2.0"
            },
            {
                "key": "traitlets",
                "package_name": "traitlets",
                "installed_version": "5.9.0",
                "required_version": ">=5.6.0"
            },
            {
                "key": "websocket-client",
                "package_name": "websocket-client",
                "installed_version": "1.5.1",
                "required_version": null
            }
        ]
    },
    {
        "package": {
            "key": "jupyter-server-terminals",
            "package_name": "jupyter-server-terminals",
            "installed_version": "0.4.4"
        },
        "dependencies": [
            {
                "key": "terminado",
                "package_name": "terminado",
                "installed_version": "0.17.1",
                "required_version": ">=0.8.3"
            }
        ]
    },
    {
        "package": {
            "key": "jupyterlab-pygments",
            "package_name": "jupyterlab-pygments",
            "installed_version": "0.2.2"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "jupyterlab-widgets",
            "package_name": "jupyterlab-widgets",
            "installed_version": "3.0.7"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "keyring",
            "package_name": "keyring",
            "installed_version": "23.13.1"
        },
        "dependencies": [
            {
                "key": "importlib-metadata",
                "package_name": "importlib-metadata",
                "installed_version": "4.13.0",
                "required_version": ">=4.11.4"
            },
            {
                "key": "jaraco.classes",
                "package_name": "jaraco.classes",
                "installed_version": "3.2.3",
                "required_version": null
            },
            {
                "key": "jeepney",
                "package_name": "jeepney",
                "installed_version": "0.8.0",
                "required_version": ">=0.4.2"
            },
            {
                "key": "secretstorage",
                "package_name": "SecretStorage",
                "installed_version": "3.3.3",
                "required_version": ">=3.2"
            }
        ]
    },
    {
        "package": {
            "key": "kiwisolver",
            "package_name": "kiwisolver",
            "installed_version": "1.4.4"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "langcodes",
            "package_name": "langcodes",
            "installed_version": "3.3.0"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "lazy-loader",
            "package_name": "lazy-loader",
            "installed_version": "0.2"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "linkify-it-py",
            "package_name": "linkify-it-py",
            "installed_version": "2.0.2"
        },
        "dependencies": [
            {
                "key": "uc-micro-py",
                "package_name": "uc-micro-py",
                "installed_version": "1.0.2",
                "required_version": null
            }
        ]
    },
    {
        "package": {
            "key": "lit",
            "package_name": "lit",
            "installed_version": "16.0.3"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "lockfile",
            "package_name": "lockfile",
            "installed_version": "0.12.2"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "markdown-it-py",
            "package_name": "markdown-it-py",
            "installed_version": "2.2.0"
        },
        "dependencies": [
            {
                "key": "mdurl",
                "package_name": "mdurl",
                "installed_version": "0.1.2",
                "required_version": "~=0.1"
            }
        ]
    },
    {
        "package": {
            "key": "markupsafe",
            "package_name": "MarkupSafe",
            "installed_version": "2.1.2"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "mathtext",
            "package_name": "mathtext",
            "installed_version": "0.0.5"
        },
        "dependencies": [
            {
                "key": "editdistance",
                "package_name": "editdistance",
                "installed_version": "0.6.2",
                "required_version": "==0.6.2"
            },
            {
                "key": "gradio",
                "package_name": "gradio",
                "installed_version": "3.31.0",
                "required_version": "==3.31.0"
            },
            {
                "key": "httpx",
                "package_name": "httpx",
                "installed_version": "0.24.0",
                "required_version": "==0.24.0"
            },
            {
                "key": "jupyter",
                "package_name": "jupyter",
                "installed_version": "1.0.0",
                "required_version": "==1.0.0"
            },
            {
                "key": "matplotlib",
                "package_name": "matplotlib",
                "installed_version": "3.7.1",
                "required_version": "==3.7.1"
            },
            {
                "key": "pandas",
                "package_name": "pandas",
                "installed_version": "2.0.1",
                "required_version": "==2.0.1"
            },
            {
                "key": "pandas-gbq",
                "package_name": "pandas-gbq",
                "installed_version": "0.19.2",
                "required_version": "==0.19.2"
            },
            {
                "key": "pytest",
                "package_name": "pytest",
                "installed_version": "7.3.1",
                "required_version": "==7.3.1"
            },
            {
                "key": "python-dotenv",
                "package_name": "python-dotenv",
                "installed_version": "1.0.0",
                "required_version": ">=0.21"
            },
            {
                "key": "scikit-image",
                "package_name": "scikit-image",
                "installed_version": "0.20.0",
                "required_version": "==0.20.0"
            },
            {
                "key": "scikit-learn",
                "package_name": "scikit-learn",
                "installed_version": "1.2.2",
                "required_version": "==1.2.2"
            },
            {
                "key": "seaborn",
                "package_name": "seaborn",
                "installed_version": "0.12.2",
                "required_version": "==0.12.2"
            },
            {
                "key": "sentence-transformers",
                "package_name": "sentence-transformers",
                "installed_version": "2.2.2",
                "required_version": "==2.2.2"
            },
            {
                "key": "spacy",
                "package_name": "spacy",
                "installed_version": "3.4.4",
                "required_version": "==3.4.4"
            },
            {
                "key": "torch",
                "package_name": "torch",
                "installed_version": "2.0.1",
                "required_version": "==2.0.1"
            },
            {
                "key": "transformers",
                "package_name": "transformers",
                "installed_version": "4.29.2",
                "required_version": "==4.29.2"
            },
            {
                "key": "unidecode",
                "package_name": "unidecode",
                "installed_version": "1.3.6",
                "required_version": "==1.3.6"
            }
        ]
    },
    {
        "package": {
            "key": "matplotlib",
            "package_name": "matplotlib",
            "installed_version": "3.7.1"
        },
        "dependencies": [
            {
                "key": "contourpy",
                "package_name": "contourpy",
                "installed_version": "1.0.7",
                "required_version": ">=1.0.1"
            },
            {
                "key": "cycler",
                "package_name": "cycler",
                "installed_version": "0.11.0",
                "required_version": ">=0.10"
            },
            {
                "key": "fonttools",
                "package_name": "fonttools",
                "installed_version": "4.39.4",
                "required_version": ">=4.22.0"
            },
            {
                "key": "importlib-resources",
                "package_name": "importlib-resources",
                "installed_version": "5.12.0",
                "required_version": ">=3.2.0"
            },
            {
                "key": "kiwisolver",
                "package_name": "kiwisolver",
                "installed_version": "1.4.4",
                "required_version": ">=1.0.1"
            },
            {
                "key": "numpy",
                "package_name": "numpy",
                "installed_version": "1.24.3",
                "required_version": ">=1.20"
            },
            {
                "key": "packaging",
                "package_name": "packaging",
                "installed_version": "23.1",
                "required_version": ">=20.0"
            },
            {
                "key": "pillow",
                "package_name": "pillow",
                "installed_version": "9.5.0",
                "required_version": ">=6.2.0"
            },
            {
                "key": "pyparsing",
                "package_name": "pyparsing",
                "installed_version": "3.0.9",
                "required_version": ">=2.3.1"
            },
            {
                "key": "python-dateutil",
                "package_name": "python-dateutil",
                "installed_version": "2.8.2",
                "required_version": ">=2.7"
            }
        ]
    },
    {
        "package": {
            "key": "matplotlib-inline",
            "package_name": "matplotlib-inline",
            "installed_version": "0.1.6"
        },
        "dependencies": [
            {
                "key": "traitlets",
                "package_name": "traitlets",
                "installed_version": "5.9.0",
                "required_version": null
            }
        ]
    },
    {
        "package": {
            "key": "mdit-py-plugins",
            "package_name": "mdit-py-plugins",
            "installed_version": "0.3.3"
        },
        "dependencies": [
            {
                "key": "markdown-it-py",
                "package_name": "markdown-it-py",
                "installed_version": "2.2.0",
                "required_version": ">=1.0.0,<3.0.0"
            }
        ]
    },
    {
        "package": {
            "key": "mdurl",
            "package_name": "mdurl",
            "installed_version": "0.1.2"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "mistune",
            "package_name": "mistune",
            "installed_version": "2.0.5"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "more-itertools",
            "package_name": "more-itertools",
            "installed_version": "9.1.0"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "mpmath",
            "package_name": "mpmath",
            "installed_version": "1.3.0"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "msgpack",
            "package_name": "msgpack",
            "installed_version": "1.0.5"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "multidict",
            "package_name": "multidict",
            "installed_version": "6.0.4"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "murmurhash",
            "package_name": "murmurhash",
            "installed_version": "1.0.9"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "nbclassic",
            "package_name": "nbclassic",
            "installed_version": "1.0.0"
        },
        "dependencies": [
            {
                "key": "argon2-cffi",
                "package_name": "argon2-cffi",
                "installed_version": "21.3.0",
                "required_version": null
            },
            {
                "key": "ipykernel",
                "package_name": "ipykernel",
                "installed_version": "6.23.1",
                "required_version": null
            },
            {
                "key": "ipython-genutils",
                "package_name": "ipython-genutils",
                "installed_version": "0.2.0",
                "required_version": null
            },
            {
                "key": "jinja2",
                "package_name": "jinja2",
                "installed_version": "3.1.2",
                "required_version": null
            },
            {
                "key": "jupyter-client",
                "package_name": "jupyter-client",
                "installed_version": "8.2.0",
                "required_version": ">=6.1.1"
            },
            {
                "key": "jupyter-core",
                "package_name": "jupyter-core",
                "installed_version": "5.3.0",
                "required_version": ">=4.6.1"
            },
            {
                "key": "jupyter-server",
                "package_name": "jupyter-server",
                "installed_version": "2.5.0",
                "required_version": ">=1.8"
            },
            {
                "key": "nbconvert",
                "package_name": "nbconvert",
                "installed_version": "7.4.0",
                "required_version": ">=5"
            },
            {
                "key": "nbformat",
                "package_name": "nbformat",
                "installed_version": "5.8.0",
                "required_version": null
            },
            {
                "key": "nest-asyncio",
                "package_name": "nest-asyncio",
                "installed_version": "1.5.6",
                "required_version": ">=1.5"
            },
            {
                "key": "notebook-shim",
                "package_name": "notebook-shim",
                "installed_version": "0.2.3",
                "required_version": ">=0.2.3"
            },
            {
                "key": "prometheus-client",
                "package_name": "prometheus-client",
                "installed_version": "0.16.0",
                "required_version": null
            },
            {
                "key": "pyzmq",
                "package_name": "pyzmq",
                "installed_version": "25.0.2",
                "required_version": ">=17"
            },
            {
                "key": "send2trash",
                "package_name": "Send2Trash",
                "installed_version": "1.8.2",
                "required_version": ">=1.8.0"
            },
            {
                "key": "terminado",
                "package_name": "terminado",
                "installed_version": "0.17.1",
                "required_version": ">=0.8.3"
            },
            {
                "key": "tornado",
                "package_name": "tornado",
                "installed_version": "6.3.2",
                "required_version": ">=6.1"
            },
            {
                "key": "traitlets",
                "package_name": "traitlets",
                "installed_version": "5.9.0",
                "required_version": ">=4.2.1"
            }
        ]
    },
    {
        "package": {
            "key": "nbclient",
            "package_name": "nbclient",
            "installed_version": "0.7.4"
        },
        "dependencies": [
            {
                "key": "jupyter-client",
                "package_name": "jupyter-client",
                "installed_version": "8.2.0",
                "required_version": ">=6.1.12"
            },
            {
                "key": "jupyter-core",
                "package_name": "jupyter-core",
                "installed_version": "5.3.0",
                "required_version": ">=4.12,!=5.0.*"
            },
            {
                "key": "nbformat",
                "package_name": "nbformat",
                "installed_version": "5.8.0",
                "required_version": ">=5.1"
            },
            {
                "key": "traitlets",
                "package_name": "traitlets",
                "installed_version": "5.9.0",
                "required_version": ">=5.3"
            }
        ]
    },
    {
        "package": {
            "key": "nbconvert",
            "package_name": "nbconvert",
            "installed_version": "7.4.0"
        },
        "dependencies": [
            {
                "key": "beautifulsoup4",
                "package_name": "beautifulsoup4",
                "installed_version": "4.12.2",
                "required_version": null
            },
            {
                "key": "bleach",
                "package_name": "bleach",
                "installed_version": "6.0.0",
                "required_version": null
            },
            {
                "key": "defusedxml",
                "package_name": "defusedxml",
                "installed_version": "0.7.1",
                "required_version": null
            },
            {
                "key": "importlib-metadata",
                "package_name": "importlib-metadata",
                "installed_version": "4.13.0",
                "required_version": ">=3.6"
            },
            {
                "key": "jinja2",
                "package_name": "jinja2",
                "installed_version": "3.1.2",
                "required_version": ">=3.0"
            },
            {
                "key": "jupyter-core",
                "package_name": "jupyter-core",
                "installed_version": "5.3.0",
                "required_version": ">=4.7"
            },
            {
                "key": "jupyterlab-pygments",
                "package_name": "jupyterlab-pygments",
                "installed_version": "0.2.2",
                "required_version": null
            },
            {
                "key": "markupsafe",
                "package_name": "markupsafe",
                "installed_version": "2.1.2",
                "required_version": ">=2.0"
            },
            {
                "key": "mistune",
                "package_name": "mistune",
                "installed_version": "2.0.5",
                "required_version": ">=2.0.3,<3"
            },
            {
                "key": "nbclient",
                "package_name": "nbclient",
                "installed_version": "0.7.4",
                "required_version": ">=0.5.0"
            },
            {
                "key": "nbformat",
                "package_name": "nbformat",
                "installed_version": "5.8.0",
                "required_version": ">=5.1"
            },
            {
                "key": "packaging",
                "package_name": "packaging",
                "installed_version": "23.1",
                "required_version": null
            },
            {
                "key": "pandocfilters",
                "package_name": "pandocfilters",
                "installed_version": "1.5.0",
                "required_version": ">=1.4.1"
            },
            {
                "key": "pygments",
                "package_name": "pygments",
                "installed_version": "2.15.1",
                "required_version": ">=2.4.1"
            },
            {
                "key": "tinycss2",
                "package_name": "tinycss2",
                "installed_version": "1.2.1",
                "required_version": null
            },
            {
                "key": "traitlets",
                "package_name": "traitlets",
                "installed_version": "5.9.0",
                "required_version": ">=5.0"
            }
        ]
    },
    {
        "package": {
            "key": "nbformat",
            "package_name": "nbformat",
            "installed_version": "5.8.0"
        },
        "dependencies": [
            {
                "key": "fastjsonschema",
                "package_name": "fastjsonschema",
                "installed_version": "2.16.3",
                "required_version": null
            },
            {
                "key": "jsonschema",
                "package_name": "jsonschema",
                "installed_version": "4.17.3",
                "required_version": ">=2.6"
            },
            {
                "key": "jupyter-core",
                "package_name": "jupyter-core",
                "installed_version": "5.3.0",
                "required_version": null
            },
            {
                "key": "traitlets",
                "package_name": "traitlets",
                "installed_version": "5.9.0",
                "required_version": ">=5.1"
            }
        ]
    },
    {
        "package": {
            "key": "nest-asyncio",
            "package_name": "nest-asyncio",
            "installed_version": "1.5.6"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "networkx",
            "package_name": "networkx",
            "installed_version": "3.1"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "nltk",
            "package_name": "nltk",
            "installed_version": "3.8.1"
        },
        "dependencies": [
            {
                "key": "click",
                "package_name": "click",
                "installed_version": "8.1.3",
                "required_version": null
            },
            {
                "key": "joblib",
                "package_name": "joblib",
                "installed_version": "1.2.0",
                "required_version": null
            },
            {
                "key": "regex",
                "package_name": "regex",
                "installed_version": "2023.5.5",
                "required_version": ">=2021.8.3"
            },
            {
                "key": "tqdm",
                "package_name": "tqdm",
                "installed_version": "4.65.0",
                "required_version": null
            }
        ]
    },
    {
        "package": {
            "key": "notebook",
            "package_name": "notebook",
            "installed_version": "6.5.4"
        },
        "dependencies": [
            {
                "key": "argon2-cffi",
                "package_name": "argon2-cffi",
                "installed_version": "21.3.0",
                "required_version": null
            },
            {
                "key": "ipykernel",
                "package_name": "ipykernel",
                "installed_version": "6.23.1",
                "required_version": null
            },
            {
                "key": "ipython-genutils",
                "package_name": "ipython-genutils",
                "installed_version": "0.2.0",
                "required_version": null
            },
            {
                "key": "jinja2",
                "package_name": "jinja2",
                "installed_version": "3.1.2",
                "required_version": null
            },
            {
                "key": "jupyter-client",
                "package_name": "jupyter-client",
                "installed_version": "8.2.0",
                "required_version": ">=5.3.4"
            },
            {
                "key": "jupyter-core",
                "package_name": "jupyter-core",
                "installed_version": "5.3.0",
                "required_version": ">=4.6.1"
            },
            {
                "key": "nbclassic",
                "package_name": "nbclassic",
                "installed_version": "1.0.0",
                "required_version": ">=0.4.7"
            },
            {
                "key": "nbconvert",
                "package_name": "nbconvert",
                "installed_version": "7.4.0",
                "required_version": ">=5"
            },
            {
                "key": "nbformat",
                "package_name": "nbformat",
                "installed_version": "5.8.0",
                "required_version": null
            },
            {
                "key": "nest-asyncio",
                "package_name": "nest-asyncio",
                "installed_version": "1.5.6",
                "required_version": ">=1.5"
            },
            {
                "key": "prometheus-client",
                "package_name": "prometheus-client",
                "installed_version": "0.16.0",
                "required_version": null
            },
            {
                "key": "pyzmq",
                "package_name": "pyzmq",
                "installed_version": "25.0.2",
                "required_version": ">=17"
            },
            {
                "key": "send2trash",
                "package_name": "Send2Trash",
                "installed_version": "1.8.2",
                "required_version": ">=1.8.0"
            },
            {
                "key": "terminado",
                "package_name": "terminado",
                "installed_version": "0.17.1",
                "required_version": ">=0.8.3"
            },
            {
                "key": "tornado",
                "package_name": "tornado",
                "installed_version": "6.3.2",
                "required_version": ">=6.1"
            },
            {
                "key": "traitlets",
                "package_name": "traitlets",
                "installed_version": "5.9.0",
                "required_version": ">=4.2.1"
            }
        ]
    },
    {
        "package": {
            "key": "notebook-shim",
            "package_name": "notebook-shim",
            "installed_version": "0.2.3"
        },
        "dependencies": [
            {
                "key": "jupyter-server",
                "package_name": "jupyter-server",
                "installed_version": "2.5.0",
                "required_version": ">=1.8,<3"
            }
        ]
    },
    {
        "package": {
            "key": "numpy",
            "package_name": "numpy",
            "installed_version": "1.24.3"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "nvidia-cublas-cu11",
            "package_name": "nvidia-cublas-cu11",
            "installed_version": "11.10.3.66"
        },
        "dependencies": [
            {
                "key": "setuptools",
                "package_name": "setuptools",
                "installed_version": "67.7.2",
                "required_version": null
            },
            {
                "key": "wheel",
                "package_name": "wheel",
                "installed_version": "0.40.0",
                "required_version": null
            }
        ]
    },
    {
        "package": {
            "key": "nvidia-cuda-cupti-cu11",
            "package_name": "nvidia-cuda-cupti-cu11",
            "installed_version": "11.7.101"
        },
        "dependencies": [
            {
                "key": "setuptools",
                "package_name": "setuptools",
                "installed_version": "67.7.2",
                "required_version": null
            },
            {
                "key": "wheel",
                "package_name": "wheel",
                "installed_version": "0.40.0",
                "required_version": null
            }
        ]
    },
    {
        "package": {
            "key": "nvidia-cuda-nvrtc-cu11",
            "package_name": "nvidia-cuda-nvrtc-cu11",
            "installed_version": "11.7.99"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "nvidia-cuda-runtime-cu11",
            "package_name": "nvidia-cuda-runtime-cu11",
            "installed_version": "11.7.99"
        },
        "dependencies": [
            {
                "key": "setuptools",
                "package_name": "setuptools",
                "installed_version": "67.7.2",
                "required_version": null
            },
            {
                "key": "wheel",
                "package_name": "wheel",
                "installed_version": "0.40.0",
                "required_version": null
            }
        ]
    },
    {
        "package": {
            "key": "nvidia-cudnn-cu11",
            "package_name": "nvidia-cudnn-cu11",
            "installed_version": "8.5.0.96"
        },
        "dependencies": [
            {
                "key": "nvidia-cublas-cu11",
                "package_name": "nvidia-cublas-cu11",
                "installed_version": "11.10.3.66",
                "required_version": null
            }
        ]
    },
    {
        "package": {
            "key": "nvidia-cufft-cu11",
            "package_name": "nvidia-cufft-cu11",
            "installed_version": "10.9.0.58"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "nvidia-curand-cu11",
            "package_name": "nvidia-curand-cu11",
            "installed_version": "10.2.10.91"
        },
        "dependencies": [
            {
                "key": "setuptools",
                "package_name": "setuptools",
                "installed_version": "67.7.2",
                "required_version": null
            },
            {
                "key": "wheel",
                "package_name": "wheel",
                "installed_version": "0.40.0",
                "required_version": null
            }
        ]
    },
    {
        "package": {
            "key": "nvidia-cusolver-cu11",
            "package_name": "nvidia-cusolver-cu11",
            "installed_version": "11.4.0.1"
        },
        "dependencies": [
            {
                "key": "nvidia-cublas-cu11",
                "package_name": "nvidia-cublas-cu11",
                "installed_version": "11.10.3.66",
                "required_version": null
            }
        ]
    },
    {
        "package": {
            "key": "nvidia-cusparse-cu11",
            "package_name": "nvidia-cusparse-cu11",
            "installed_version": "11.7.4.91"
        },
        "dependencies": [
            {
                "key": "setuptools",
                "package_name": "setuptools",
                "installed_version": "67.7.2",
                "required_version": null
            },
            {
                "key": "wheel",
                "package_name": "wheel",
                "installed_version": "0.40.0",
                "required_version": null
            }
        ]
    },
    {
        "package": {
            "key": "nvidia-nccl-cu11",
            "package_name": "nvidia-nccl-cu11",
            "installed_version": "2.14.3"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "nvidia-nvtx-cu11",
            "package_name": "nvidia-nvtx-cu11",
            "installed_version": "11.7.91"
        },
        "dependencies": [
            {
                "key": "setuptools",
                "package_name": "setuptools",
                "installed_version": "67.7.2",
                "required_version": null
            },
            {
                "key": "wheel",
                "package_name": "wheel",
                "installed_version": "0.40.0",
                "required_version": null
            }
        ]
    },
    {
        "package": {
            "key": "oauthlib",
            "package_name": "oauthlib",
            "installed_version": "3.2.2"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "orjson",
            "package_name": "orjson",
            "installed_version": "3.8.12"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "packaging",
            "package_name": "packaging",
            "installed_version": "23.1"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "pandas",
            "package_name": "pandas",
            "installed_version": "2.0.1"
        },
        "dependencies": [
            {
                "key": "numpy",
                "package_name": "numpy",
                "installed_version": "1.24.3",
                "required_version": ">=1.20.3"
            },
            {
                "key": "python-dateutil",
                "package_name": "python-dateutil",
                "installed_version": "2.8.2",
                "required_version": ">=2.8.2"
            },
            {
                "key": "pytz",
                "package_name": "pytz",
                "installed_version": "2023.3",
                "required_version": ">=2020.1"
            },
            {
                "key": "tzdata",
                "package_name": "tzdata",
                "installed_version": "2023.3",
                "required_version": ">=2022.1"
            }
        ]
    },
    {
        "package": {
            "key": "pandas-gbq",
            "package_name": "pandas-gbq",
            "installed_version": "0.19.2"
        },
        "dependencies": [
            {
                "key": "db-dtypes",
                "package_name": "db-dtypes",
                "installed_version": "1.1.1",
                "required_version": ">=1.0.4,<2.0.0"
            },
            {
                "key": "google-api-core",
                "package_name": "google-api-core",
                "installed_version": "2.11.0",
                "required_version": ">=2.10.2,<3.0.0dev"
            },
            {
                "key": "google-auth",
                "package_name": "google-auth",
                "installed_version": "2.18.0",
                "required_version": ">=2.13.0"
            },
            {
                "key": "google-auth-oauthlib",
                "package_name": "google-auth-oauthlib",
                "installed_version": "1.0.0",
                "required_version": ">=0.7.0"
            },
            {
                "key": "google-cloud-bigquery",
                "package_name": "google-cloud-bigquery",
                "installed_version": "3.10.0",
                "required_version": ">=3.3.5,<4.0.0dev,!=2.4.*"
            },
            {
                "key": "google-cloud-bigquery-storage",
                "package_name": "google-cloud-bigquery-storage",
                "installed_version": "2.19.1",
                "required_version": ">=2.16.2,<3.0.0dev"
            },
            {
                "key": "numpy",
                "package_name": "numpy",
                "installed_version": "1.24.3",
                "required_version": ">=1.16.6"
            },
            {
                "key": "pandas",
                "package_name": "pandas",
                "installed_version": "2.0.1",
                "required_version": ">=1.1.4"
            },
            {
                "key": "pyarrow",
                "package_name": "pyarrow",
                "installed_version": "12.0.0",
                "required_version": ">=3.0.0"
            },
            {
                "key": "pydata-google-auth",
                "package_name": "pydata-google-auth",
                "installed_version": "1.8.0",
                "required_version": ">=1.5.0"
            },
            {
                "key": "setuptools",
                "package_name": "setuptools",
                "installed_version": "67.7.2",
                "required_version": null
            }
        ]
    },
    {
        "package": {
            "key": "pandocfilters",
            "package_name": "pandocfilters",
            "installed_version": "1.5.0"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "parso",
            "package_name": "parso",
            "installed_version": "0.8.3"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "pathy",
            "package_name": "pathy",
            "installed_version": "0.10.1"
        },
        "dependencies": [
            {
                "key": "smart-open",
                "package_name": "smart-open",
                "installed_version": "6.3.0",
                "required_version": ">=5.2.1,<7.0.0"
            },
            {
                "key": "typer",
                "package_name": "typer",
                "installed_version": "0.7.0",
                "required_version": ">=0.3.0,<1.0.0"
            }
        ]
    },
    {
        "package": {
            "key": "pexpect",
            "package_name": "pexpect",
            "installed_version": "4.8.0"
        },
        "dependencies": [
            {
                "key": "ptyprocess",
                "package_name": "ptyprocess",
                "installed_version": "0.7.0",
                "required_version": ">=0.5"
            }
        ]
    },
    {
        "package": {
            "key": "pickleshare",
            "package_name": "pickleshare",
            "installed_version": "0.7.5"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "pillow",
            "package_name": "Pillow",
            "installed_version": "9.5.0"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "pip",
            "package_name": "pip",
            "installed_version": "23.1.2"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "pipdeptree",
            "package_name": "pipdeptree",
            "installed_version": "2.7.1"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "pkginfo",
            "package_name": "pkginfo",
            "installed_version": "1.9.6"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "platformdirs",
            "package_name": "platformdirs",
            "installed_version": "2.6.2"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "pluggy",
            "package_name": "pluggy",
            "installed_version": "1.0.0"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "poetry",
            "package_name": "poetry",
            "installed_version": "1.3.2"
        },
        "dependencies": [
            {
                "key": "cachecontrol",
                "package_name": "cachecontrol",
                "installed_version": "0.12.11",
                "required_version": ">=0.12.9,<0.13.0"
            },
            {
                "key": "cleo",
                "package_name": "cleo",
                "installed_version": "2.0.1",
                "required_version": ">=2.0.0,<3.0.0"
            },
            {
                "key": "crashtest",
                "package_name": "crashtest",
                "installed_version": "0.4.1",
                "required_version": ">=0.4.1,<0.5.0"
            },
            {
                "key": "dulwich",
                "package_name": "dulwich",
                "installed_version": "0.20.50",
                "required_version": ">=0.20.46,<0.21.0"
            },
            {
                "key": "filelock",
                "package_name": "filelock",
                "installed_version": "3.12.0",
                "required_version": ">=3.8.0,<4.0.0"
            },
            {
                "key": "html5lib",
                "package_name": "html5lib",
                "installed_version": "1.1",
                "required_version": ">=1.0,<2.0"
            },
            {
                "key": "importlib-metadata",
                "package_name": "importlib-metadata",
                "installed_version": "4.13.0",
                "required_version": ">=4.4,<5.0"
            },
            {
                "key": "jsonschema",
                "package_name": "jsonschema",
                "installed_version": "4.17.3",
                "required_version": ">=4.10.0,<5.0.0"
            },
            {
                "key": "keyring",
                "package_name": "keyring",
                "installed_version": "23.13.1",
                "required_version": ">=23.9.0,<24.0.0"
            },
            {
                "key": "lockfile",
                "package_name": "lockfile",
                "installed_version": "0.12.2",
                "required_version": ">=0.12.2,<0.13.0"
            },
            {
                "key": "packaging",
                "package_name": "packaging",
                "installed_version": "23.1",
                "required_version": ">=20.4"
            },
            {
                "key": "pexpect",
                "package_name": "pexpect",
                "installed_version": "4.8.0",
                "required_version": ">=4.7.0,<5.0.0"
            },
            {
                "key": "pkginfo",
                "package_name": "pkginfo",
                "installed_version": "1.9.6",
                "required_version": ">=1.5,<2.0"
            },
            {
                "key": "platformdirs",
                "package_name": "platformdirs",
                "installed_version": "2.6.2",
                "required_version": ">=2.5.2,<3.0.0"
            },
            {
                "key": "poetry-core",
                "package_name": "poetry-core",
                "installed_version": "1.4.0",
                "required_version": "==1.4.0"
            },
            {
                "key": "poetry-plugin-export",
                "package_name": "poetry-plugin-export",
                "installed_version": "1.3.1",
                "required_version": ">=1.2.0,<2.0.0"
            },
            {
                "key": "requests",
                "package_name": "requests",
                "installed_version": "2.30.0",
                "required_version": ">=2.18,<3.0"
            },
            {
                "key": "requests-toolbelt",
                "package_name": "requests-toolbelt",
                "installed_version": "0.10.1",
                "required_version": ">=0.9.1,<0.11.0"
            },
            {
                "key": "shellingham",
                "package_name": "shellingham",
                "installed_version": "1.5.0.post1",
                "required_version": ">=1.5,<2.0"
            },
            {
                "key": "tomli",
                "package_name": "tomli",
                "installed_version": "2.0.1",
                "required_version": ">=2.0.1,<3.0.0"
            },
            {
                "key": "tomlkit",
                "package_name": "tomlkit",
                "installed_version": "0.11.8",
                "required_version": ">=0.11.1,<1.0.0,!=0.11.3,!=0.11.2"
            },
            {
                "key": "trove-classifiers",
                "package_name": "trove-classifiers",
                "installed_version": "2023.5.2",
                "required_version": ">=2022.5.19"
            },
            {
                "key": "urllib3",
                "package_name": "urllib3",
                "installed_version": "1.26.15",
                "required_version": ">=1.26.0,<2.0.0"
            },
            {
                "key": "virtualenv",
                "package_name": "virtualenv",
                "installed_version": "20.21.1",
                "required_version": ">=20.4.3,<21.0.0,!=20.4.6,!=20.4.5"
            }
        ]
    },
    {
        "package": {
            "key": "poetry-core",
            "package_name": "poetry-core",
            "installed_version": "1.4.0"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "poetry-plugin-export",
            "package_name": "poetry-plugin-export",
            "installed_version": "1.3.1"
        },
        "dependencies": [
            {
                "key": "poetry",
                "package_name": "poetry",
                "installed_version": "1.3.2",
                "required_version": ">=1.3.0,<2.0.0"
            },
            {
                "key": "poetry-core",
                "package_name": "poetry-core",
                "installed_version": "1.4.0",
                "required_version": ">=1.3.0,<2.0.0"
            }
        ]
    },
    {
        "package": {
            "key": "preshed",
            "package_name": "preshed",
            "installed_version": "3.0.8"
        },
        "dependencies": [
            {
                "key": "cymem",
                "package_name": "cymem",
                "installed_version": "2.0.7",
                "required_version": ">=2.0.2,<2.1.0"
            },
            {
                "key": "murmurhash",
                "package_name": "murmurhash",
                "installed_version": "1.0.9",
                "required_version": ">=0.28.0,<1.1.0"
            }
        ]
    },
    {
        "package": {
            "key": "prometheus-client",
            "package_name": "prometheus-client",
            "installed_version": "0.16.0"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "prompt-toolkit",
            "package_name": "prompt-toolkit",
            "installed_version": "3.0.38"
        },
        "dependencies": [
            {
                "key": "wcwidth",
                "package_name": "wcwidth",
                "installed_version": "0.2.6",
                "required_version": null
            }
        ]
    },
    {
        "package": {
            "key": "proto-plus",
            "package_name": "proto-plus",
            "installed_version": "1.22.2"
        },
        "dependencies": [
            {
                "key": "protobuf",
                "package_name": "protobuf",
                "installed_version": "4.23.0",
                "required_version": ">=3.19.0,<5.0.0dev"
            }
        ]
    },
    {
        "package": {
            "key": "protobuf",
            "package_name": "protobuf",
            "installed_version": "4.23.0"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "psutil",
            "package_name": "psutil",
            "installed_version": "5.9.5"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "ptyprocess",
            "package_name": "ptyprocess",
            "installed_version": "0.7.0"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "pure-eval",
            "package_name": "pure-eval",
            "installed_version": "0.2.2"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "pyarrow",
            "package_name": "pyarrow",
            "installed_version": "12.0.0"
        },
        "dependencies": [
            {
                "key": "numpy",
                "package_name": "numpy",
                "installed_version": "1.24.3",
                "required_version": ">=1.16.6"
            }
        ]
    },
    {
        "package": {
            "key": "pyasn1",
            "package_name": "pyasn1",
            "installed_version": "0.5.0"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "pyasn1-modules",
            "package_name": "pyasn1-modules",
            "installed_version": "0.3.0"
        },
        "dependencies": [
            {
                "key": "pyasn1",
                "package_name": "pyasn1",
                "installed_version": "0.5.0",
                "required_version": ">=0.4.6,<0.6.0"
            }
        ]
    },
    {
        "package": {
            "key": "pycparser",
            "package_name": "pycparser",
            "installed_version": "2.21"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "pydantic",
            "package_name": "pydantic",
            "installed_version": "1.10.7"
        },
        "dependencies": [
            {
                "key": "typing-extensions",
                "package_name": "typing-extensions",
                "installed_version": "4.5.0",
                "required_version": ">=4.2.0"
            }
        ]
    },
    {
        "package": {
            "key": "pydata-google-auth",
            "package_name": "pydata-google-auth",
            "installed_version": "1.8.0"
        },
        "dependencies": [
            {
                "key": "google-auth",
                "package_name": "google-auth",
                "installed_version": "2.18.0",
                "required_version": ">=1.25.0,<3.0dev"
            },
            {
                "key": "google-auth-oauthlib",
                "package_name": "google-auth-oauthlib",
                "installed_version": "1.0.0",
                "required_version": ">=0.4.0"
            },
            {
                "key": "setuptools",
                "package_name": "setuptools",
                "installed_version": "67.7.2",
                "required_version": null
            }
        ]
    },
    {
        "package": {
            "key": "pydub",
            "package_name": "pydub",
            "installed_version": "0.25.1"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "pygments",
            "package_name": "Pygments",
            "installed_version": "2.15.1"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "pyparsing",
            "package_name": "pyparsing",
            "installed_version": "3.0.9"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "pyproject-hooks",
            "package_name": "pyproject-hooks",
            "installed_version": "1.0.0"
        },
        "dependencies": [
            {
                "key": "tomli",
                "package_name": "tomli",
                "installed_version": "2.0.1",
                "required_version": ">=1.1.0"
            }
        ]
    },
    {
        "package": {
            "key": "pyrsistent",
            "package_name": "pyrsistent",
            "installed_version": "0.19.3"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "pytest",
            "package_name": "pytest",
            "installed_version": "7.3.1"
        },
        "dependencies": [
            {
                "key": "exceptiongroup",
                "package_name": "exceptiongroup",
                "installed_version": "1.1.1",
                "required_version": ">=1.0.0rc8"
            },
            {
                "key": "iniconfig",
                "package_name": "iniconfig",
                "installed_version": "2.0.0",
                "required_version": null
            },
            {
                "key": "packaging",
                "package_name": "packaging",
                "installed_version": "23.1",
                "required_version": null
            },
            {
                "key": "pluggy",
                "package_name": "pluggy",
                "installed_version": "1.0.0",
                "required_version": ">=0.12,<2.0"
            },
            {
                "key": "tomli",
                "package_name": "tomli",
                "installed_version": "2.0.1",
                "required_version": ">=1.0.0"
            }
        ]
    },
    {
        "package": {
            "key": "python-dateutil",
            "package_name": "python-dateutil",
            "installed_version": "2.8.2"
        },
        "dependencies": [
            {
                "key": "six",
                "package_name": "six",
                "installed_version": "1.16.0",
                "required_version": ">=1.5"
            }
        ]
    },
    {
        "package": {
            "key": "python-dotenv",
            "package_name": "python-dotenv",
            "installed_version": "1.0.0"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "python-json-logger",
            "package_name": "python-json-logger",
            "installed_version": "2.0.7"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "python-multipart",
            "package_name": "python-multipart",
            "installed_version": "0.0.6"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "pytz",
            "package_name": "pytz",
            "installed_version": "2023.3"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "pywavelets",
            "package_name": "PyWavelets",
            "installed_version": "1.4.1"
        },
        "dependencies": [
            {
                "key": "numpy",
                "package_name": "numpy",
                "installed_version": "1.24.3",
                "required_version": ">=1.17.3"
            }
        ]
    },
    {
        "package": {
            "key": "pyyaml",
            "package_name": "PyYAML",
            "installed_version": "6.0"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "pyzmq",
            "package_name": "pyzmq",
            "installed_version": "25.0.2"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "qtconsole",
            "package_name": "qtconsole",
            "installed_version": "5.4.3"
        },
        "dependencies": [
            {
                "key": "ipykernel",
                "package_name": "ipykernel",
                "installed_version": "6.23.1",
                "required_version": ">=4.1"
            },
            {
                "key": "ipython-genutils",
                "package_name": "ipython-genutils",
                "installed_version": "0.2.0",
                "required_version": null
            },
            {
                "key": "jupyter-client",
                "package_name": "jupyter-client",
                "installed_version": "8.2.0",
                "required_version": ">=4.1"
            },
            {
                "key": "jupyter-core",
                "package_name": "jupyter-core",
                "installed_version": "5.3.0",
                "required_version": null
            },
            {
                "key": "packaging",
                "package_name": "packaging",
                "installed_version": "23.1",
                "required_version": null
            },
            {
                "key": "pygments",
                "package_name": "pygments",
                "installed_version": "2.15.1",
                "required_version": null
            },
            {
                "key": "pyzmq",
                "package_name": "pyzmq",
                "installed_version": "25.0.2",
                "required_version": ">=17.1"
            },
            {
                "key": "qtpy",
                "package_name": "qtpy",
                "installed_version": "2.3.1",
                "required_version": ">=2.0.1"
            },
            {
                "key": "traitlets",
                "package_name": "traitlets",
                "installed_version": "5.9.0",
                "required_version": "!=5.2.2,!=5.2.1"
            }
        ]
    },
    {
        "package": {
            "key": "qtpy",
            "package_name": "QtPy",
            "installed_version": "2.3.1"
        },
        "dependencies": [
            {
                "key": "packaging",
                "package_name": "packaging",
                "installed_version": "23.1",
                "required_version": null
            }
        ]
    },
    {
        "package": {
            "key": "rapidfuzz",
            "package_name": "rapidfuzz",
            "installed_version": "2.15.1"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "readme-renderer",
            "package_name": "readme-renderer",
            "installed_version": "37.3"
        },
        "dependencies": [
            {
                "key": "bleach",
                "package_name": "bleach",
                "installed_version": "6.0.0",
                "required_version": ">=2.1.0"
            },
            {
                "key": "docutils",
                "package_name": "docutils",
                "installed_version": "0.20.1",
                "required_version": ">=0.13.1"
            },
            {
                "key": "pygments",
                "package_name": "Pygments",
                "installed_version": "2.15.1",
                "required_version": ">=2.5.1"
            }
        ]
    },
    {
        "package": {
            "key": "regex",
            "package_name": "regex",
            "installed_version": "2023.5.5"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "requests",
            "package_name": "requests",
            "installed_version": "2.30.0"
        },
        "dependencies": [
            {
                "key": "certifi",
                "package_name": "certifi",
                "installed_version": "2023.5.7",
                "required_version": ">=2017.4.17"
            },
            {
                "key": "charset-normalizer",
                "package_name": "charset-normalizer",
                "installed_version": "3.1.0",
                "required_version": ">=2,<4"
            },
            {
                "key": "idna",
                "package_name": "idna",
                "installed_version": "3.4",
                "required_version": ">=2.5,<4"
            },
            {
                "key": "urllib3",
                "package_name": "urllib3",
                "installed_version": "1.26.15",
                "required_version": ">=1.21.1,<3"
            }
        ]
    },
    {
        "package": {
            "key": "requests-oauthlib",
            "package_name": "requests-oauthlib",
            "installed_version": "1.3.1"
        },
        "dependencies": [
            {
                "key": "oauthlib",
                "package_name": "oauthlib",
                "installed_version": "3.2.2",
                "required_version": ">=3.0.0"
            },
            {
                "key": "requests",
                "package_name": "requests",
                "installed_version": "2.30.0",
                "required_version": ">=2.0.0"
            }
        ]
    },
    {
        "package": {
            "key": "requests-toolbelt",
            "package_name": "requests-toolbelt",
            "installed_version": "0.10.1"
        },
        "dependencies": [
            {
                "key": "requests",
                "package_name": "requests",
                "installed_version": "2.30.0",
                "required_version": ">=2.0.1,<3.0.0"
            }
        ]
    },
    {
        "package": {
            "key": "rfc3339-validator",
            "package_name": "rfc3339-validator",
            "installed_version": "0.1.4"
        },
        "dependencies": [
            {
                "key": "six",
                "package_name": "six",
                "installed_version": "1.16.0",
                "required_version": null
            }
        ]
    },
    {
        "package": {
            "key": "rfc3986",
            "package_name": "rfc3986",
            "installed_version": "2.0.0"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "rfc3986-validator",
            "package_name": "rfc3986-validator",
            "installed_version": "0.1.1"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "rich",
            "package_name": "rich",
            "installed_version": "13.3.5"
        },
        "dependencies": [
            {
                "key": "markdown-it-py",
                "package_name": "markdown-it-py",
                "installed_version": "2.2.0",
                "required_version": ">=2.2.0,<3.0.0"
            },
            {
                "key": "pygments",
                "package_name": "pygments",
                "installed_version": "2.15.1",
                "required_version": ">=2.13.0,<3.0.0"
            }
        ]
    },
    {
        "package": {
            "key": "rsa",
            "package_name": "rsa",
            "installed_version": "4.9"
        },
        "dependencies": [
            {
                "key": "pyasn1",
                "package_name": "pyasn1",
                "installed_version": "0.5.0",
                "required_version": ">=0.1.3"
            }
        ]
    },
    {
        "package": {
            "key": "scikit-image",
            "package_name": "scikit-image",
            "installed_version": "0.20.0"
        },
        "dependencies": [
            {
                "key": "imageio",
                "package_name": "imageio",
                "installed_version": "2.28.1",
                "required_version": ">=2.4.1"
            },
            {
                "key": "lazy-loader",
                "package_name": "lazy-loader",
                "installed_version": "0.2",
                "required_version": ">=0.1"
            },
            {
                "key": "networkx",
                "package_name": "networkx",
                "installed_version": "3.1",
                "required_version": ">=2.8"
            },
            {
                "key": "numpy",
                "package_name": "numpy",
                "installed_version": "1.24.3",
                "required_version": ">=1.21.1"
            },
            {
                "key": "packaging",
                "package_name": "packaging",
                "installed_version": "23.1",
                "required_version": ">=20.0"
            },
            {
                "key": "pillow",
                "package_name": "pillow",
                "installed_version": "9.5.0",
                "required_version": ">=9.0.1"
            },
            {
                "key": "pywavelets",
                "package_name": "PyWavelets",
                "installed_version": "1.4.1",
                "required_version": ">=1.1.1"
            },
            {
                "key": "scipy",
                "package_name": "scipy",
                "installed_version": "1.9.1",
                "required_version": ">=1.8,<1.9.2"
            },
            {
                "key": "tifffile",
                "package_name": "tifffile",
                "installed_version": "2023.4.12",
                "required_version": ">=2019.7.26"
            }
        ]
    },
    {
        "package": {
            "key": "scikit-learn",
            "package_name": "scikit-learn",
            "installed_version": "1.2.2"
        },
        "dependencies": [
            {
                "key": "joblib",
                "package_name": "joblib",
                "installed_version": "1.2.0",
                "required_version": ">=1.1.1"
            },
            {
                "key": "numpy",
                "package_name": "numpy",
                "installed_version": "1.24.3",
                "required_version": ">=1.17.3"
            },
            {
                "key": "scipy",
                "package_name": "scipy",
                "installed_version": "1.9.1",
                "required_version": ">=1.3.2"
            },
            {
                "key": "threadpoolctl",
                "package_name": "threadpoolctl",
                "installed_version": "3.1.0",
                "required_version": ">=2.0.0"
            }
        ]
    },
    {
        "package": {
            "key": "scipy",
            "package_name": "scipy",
            "installed_version": "1.9.1"
        },
        "dependencies": [
            {
                "key": "numpy",
                "package_name": "numpy",
                "installed_version": "1.24.3",
                "required_version": ">=1.18.5,<1.25.0"
            }
        ]
    },
    {
        "package": {
            "key": "seaborn",
            "package_name": "seaborn",
            "installed_version": "0.12.2"
        },
        "dependencies": [
            {
                "key": "matplotlib",
                "package_name": "matplotlib",
                "installed_version": "3.7.1",
                "required_version": ">=3.1,!=3.6.1"
            },
            {
                "key": "numpy",
                "package_name": "numpy",
                "installed_version": "1.24.3",
                "required_version": ">=1.17,!=1.24.0"
            },
            {
                "key": "pandas",
                "package_name": "pandas",
                "installed_version": "2.0.1",
                "required_version": ">=0.25"
            }
        ]
    },
    {
        "package": {
            "key": "secretstorage",
            "package_name": "SecretStorage",
            "installed_version": "3.3.3"
        },
        "dependencies": [
            {
                "key": "cryptography",
                "package_name": "cryptography",
                "installed_version": "40.0.2",
                "required_version": ">=2.0"
            },
            {
                "key": "jeepney",
                "package_name": "jeepney",
                "installed_version": "0.8.0",
                "required_version": ">=0.6"
            }
        ]
    },
    {
        "package": {
            "key": "semantic-version",
            "package_name": "semantic-version",
            "installed_version": "2.10.0"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "send2trash",
            "package_name": "Send2Trash",
            "installed_version": "1.8.2"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "sentence-transformers",
            "package_name": "sentence-transformers",
            "installed_version": "2.2.2"
        },
        "dependencies": [
            {
                "key": "huggingface-hub",
                "package_name": "huggingface-hub",
                "installed_version": "0.14.1",
                "required_version": ">=0.4.0"
            },
            {
                "key": "nltk",
                "package_name": "nltk",
                "installed_version": "3.8.1",
                "required_version": null
            },
            {
                "key": "numpy",
                "package_name": "numpy",
                "installed_version": "1.24.3",
                "required_version": null
            },
            {
                "key": "scikit-learn",
                "package_name": "scikit-learn",
                "installed_version": "1.2.2",
                "required_version": null
            },
            {
                "key": "scipy",
                "package_name": "scipy",
                "installed_version": "1.9.1",
                "required_version": null
            },
            {
                "key": "sentencepiece",
                "package_name": "sentencepiece",
                "installed_version": "0.1.99",
                "required_version": null
            },
            {
                "key": "torch",
                "package_name": "torch",
                "installed_version": "2.0.1",
                "required_version": ">=1.6.0"
            },
            {
                "key": "torchvision",
                "package_name": "torchvision",
                "installed_version": "0.15.2",
                "required_version": null
            },
            {
                "key": "tqdm",
                "package_name": "tqdm",
                "installed_version": "4.65.0",
                "required_version": null
            },
            {
                "key": "transformers",
                "package_name": "transformers",
                "installed_version": "4.29.2",
                "required_version": ">=4.6.0,<5.0.0"
            }
        ]
    },
    {
        "package": {
            "key": "sentencepiece",
            "package_name": "sentencepiece",
            "installed_version": "0.1.99"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "setuptools",
            "package_name": "setuptools",
            "installed_version": "67.7.2"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "shellingham",
            "package_name": "shellingham",
            "installed_version": "1.5.0.post1"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "six",
            "package_name": "six",
            "installed_version": "1.16.0"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "smart-open",
            "package_name": "smart-open",
            "installed_version": "6.3.0"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "sniffio",
            "package_name": "sniffio",
            "installed_version": "1.3.0"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "soupsieve",
            "package_name": "soupsieve",
            "installed_version": "2.4.1"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "spacy",
            "package_name": "spacy",
            "installed_version": "3.4.4"
        },
        "dependencies": [
            {
                "key": "catalogue",
                "package_name": "catalogue",
                "installed_version": "2.0.8",
                "required_version": ">=2.0.6,<2.1.0"
            },
            {
                "key": "cymem",
                "package_name": "cymem",
                "installed_version": "2.0.7",
                "required_version": ">=2.0.2,<2.1.0"
            },
            {
                "key": "jinja2",
                "package_name": "jinja2",
                "installed_version": "3.1.2",
                "required_version": null
            },
            {
                "key": "langcodes",
                "package_name": "langcodes",
                "installed_version": "3.3.0",
                "required_version": ">=3.2.0,<4.0.0"
            },
            {
                "key": "murmurhash",
                "package_name": "murmurhash",
                "installed_version": "1.0.9",
                "required_version": ">=0.28.0,<1.1.0"
            },
            {
                "key": "numpy",
                "package_name": "numpy",
                "installed_version": "1.24.3",
                "required_version": ">=1.15.0"
            },
            {
                "key": "packaging",
                "package_name": "packaging",
                "installed_version": "23.1",
                "required_version": ">=20.0"
            },
            {
                "key": "pathy",
                "package_name": "pathy",
                "installed_version": "0.10.1",
                "required_version": ">=0.3.5"
            },
            {
                "key": "preshed",
                "package_name": "preshed",
                "installed_version": "3.0.8",
                "required_version": ">=3.0.2,<3.1.0"
            },
            {
                "key": "pydantic",
                "package_name": "pydantic",
                "installed_version": "1.10.7",
                "required_version": ">=1.7.4,<1.11.0,!=1.8.1,!=1.8"
            },
            {
                "key": "requests",
                "package_name": "requests",
                "installed_version": "2.30.0",
                "required_version": ">=2.13.0,<3.0.0"
            },
            {
                "key": "setuptools",
                "package_name": "setuptools",
                "installed_version": "67.7.2",
                "required_version": null
            },
            {
                "key": "smart-open",
                "package_name": "smart-open",
                "installed_version": "6.3.0",
                "required_version": ">=5.2.1,<7.0.0"
            },
            {
                "key": "spacy-legacy",
                "package_name": "spacy-legacy",
                "installed_version": "3.0.12",
                "required_version": ">=3.0.10,<3.1.0"
            },
            {
                "key": "spacy-loggers",
                "package_name": "spacy-loggers",
                "installed_version": "1.0.4",
                "required_version": ">=1.0.0,<2.0.0"
            },
            {
                "key": "srsly",
                "package_name": "srsly",
                "installed_version": "2.4.6",
                "required_version": ">=2.4.3,<3.0.0"
            },
            {
                "key": "thinc",
                "package_name": "thinc",
                "installed_version": "8.1.10",
                "required_version": ">=8.1.0,<8.2.0"
            },
            {
                "key": "tqdm",
                "package_name": "tqdm",
                "installed_version": "4.65.0",
                "required_version": ">=4.38.0,<5.0.0"
            },
            {
                "key": "typer",
                "package_name": "typer",
                "installed_version": "0.7.0",
                "required_version": ">=0.3.0,<0.8.0"
            },
            {
                "key": "wasabi",
                "package_name": "wasabi",
                "installed_version": "0.10.1",
                "required_version": ">=0.9.1,<1.1.0"
            }
        ]
    },
    {
        "package": {
            "key": "spacy-legacy",
            "package_name": "spacy-legacy",
            "installed_version": "3.0.12"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "spacy-loggers",
            "package_name": "spacy-loggers",
            "installed_version": "1.0.4"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "srsly",
            "package_name": "srsly",
            "installed_version": "2.4.6"
        },
        "dependencies": [
            {
                "key": "catalogue",
                "package_name": "catalogue",
                "installed_version": "2.0.8",
                "required_version": ">=2.0.3,<2.1.0"
            }
        ]
    },
    {
        "package": {
            "key": "stack-data",
            "package_name": "stack-data",
            "installed_version": "0.6.2"
        },
        "dependencies": [
            {
                "key": "asttokens",
                "package_name": "asttokens",
                "installed_version": "2.2.1",
                "required_version": ">=2.1.0"
            },
            {
                "key": "executing",
                "package_name": "executing",
                "installed_version": "1.2.0",
                "required_version": ">=1.2.0"
            },
            {
                "key": "pure-eval",
                "package_name": "pure-eval",
                "installed_version": "0.2.2",
                "required_version": null
            }
        ]
    },
    {
        "package": {
            "key": "starlette",
            "package_name": "starlette",
            "installed_version": "0.27.0"
        },
        "dependencies": [
            {
                "key": "anyio",
                "package_name": "anyio",
                "installed_version": "3.6.2",
                "required_version": ">=3.4.0,<5"
            },
            {
                "key": "typing-extensions",
                "package_name": "typing-extensions",
                "installed_version": "4.5.0",
                "required_version": ">=3.10.0"
            }
        ]
    },
    {
        "package": {
            "key": "sympy",
            "package_name": "sympy",
            "installed_version": "1.12"
        },
        "dependencies": [
            {
                "key": "mpmath",
                "package_name": "mpmath",
                "installed_version": "1.3.0",
                "required_version": ">=0.19"
            }
        ]
    },
    {
        "package": {
            "key": "terminado",
            "package_name": "terminado",
            "installed_version": "0.17.1"
        },
        "dependencies": [
            {
                "key": "ptyprocess",
                "package_name": "ptyprocess",
                "installed_version": "0.7.0",
                "required_version": null
            },
            {
                "key": "tornado",
                "package_name": "tornado",
                "installed_version": "6.3.2",
                "required_version": ">=6.1.0"
            }
        ]
    },
    {
        "package": {
            "key": "thinc",
            "package_name": "thinc",
            "installed_version": "8.1.10"
        },
        "dependencies": [
            {
                "key": "blis",
                "package_name": "blis",
                "installed_version": "0.7.9",
                "required_version": ">=0.7.8,<0.8.0"
            },
            {
                "key": "catalogue",
                "package_name": "catalogue",
                "installed_version": "2.0.8",
                "required_version": ">=2.0.4,<2.1.0"
            },
            {
                "key": "confection",
                "package_name": "confection",
                "installed_version": "0.0.4",
                "required_version": ">=0.0.1,<1.0.0"
            },
            {
                "key": "cymem",
                "package_name": "cymem",
                "installed_version": "2.0.7",
                "required_version": ">=2.0.2,<2.1.0"
            },
            {
                "key": "murmurhash",
                "package_name": "murmurhash",
                "installed_version": "1.0.9",
                "required_version": ">=1.0.2,<1.1.0"
            },
            {
                "key": "numpy",
                "package_name": "numpy",
                "installed_version": "1.24.3",
                "required_version": ">=1.15.0"
            },
            {
                "key": "packaging",
                "package_name": "packaging",
                "installed_version": "23.1",
                "required_version": ">=20.0"
            },
            {
                "key": "preshed",
                "package_name": "preshed",
                "installed_version": "3.0.8",
                "required_version": ">=3.0.2,<3.1.0"
            },
            {
                "key": "pydantic",
                "package_name": "pydantic",
                "installed_version": "1.10.7",
                "required_version": ">=1.7.4,<1.11.0,!=1.8.1,!=1.8"
            },
            {
                "key": "setuptools",
                "package_name": "setuptools",
                "installed_version": "67.7.2",
                "required_version": null
            },
            {
                "key": "srsly",
                "package_name": "srsly",
                "installed_version": "2.4.6",
                "required_version": ">=2.4.0,<3.0.0"
            },
            {
                "key": "wasabi",
                "package_name": "wasabi",
                "installed_version": "0.10.1",
                "required_version": ">=0.8.1,<1.2.0"
            }
        ]
    },
    {
        "package": {
            "key": "threadpoolctl",
            "package_name": "threadpoolctl",
            "installed_version": "3.1.0"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "tifffile",
            "package_name": "tifffile",
            "installed_version": "2023.4.12"
        },
        "dependencies": [
            {
                "key": "numpy",
                "package_name": "numpy",
                "installed_version": "1.24.3",
                "required_version": null
            }
        ]
    },
    {
        "package": {
            "key": "tinycss2",
            "package_name": "tinycss2",
            "installed_version": "1.2.1"
        },
        "dependencies": [
            {
                "key": "webencodings",
                "package_name": "webencodings",
                "installed_version": "0.5.1",
                "required_version": ">=0.4"
            }
        ]
    },
    {
        "package": {
            "key": "tokenizers",
            "package_name": "tokenizers",
            "installed_version": "0.13.3"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "tomli",
            "package_name": "tomli",
            "installed_version": "2.0.1"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "tomlkit",
            "package_name": "tomlkit",
            "installed_version": "0.11.8"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "toolz",
            "package_name": "toolz",
            "installed_version": "0.12.0"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "torch",
            "package_name": "torch",
            "installed_version": "2.0.1"
        },
        "dependencies": [
            {
                "key": "filelock",
                "package_name": "filelock",
                "installed_version": "3.12.0",
                "required_version": null
            },
            {
                "key": "jinja2",
                "package_name": "jinja2",
                "installed_version": "3.1.2",
                "required_version": null
            },
            {
                "key": "networkx",
                "package_name": "networkx",
                "installed_version": "3.1",
                "required_version": null
            },
            {
                "key": "nvidia-cublas-cu11",
                "package_name": "nvidia-cublas-cu11",
                "installed_version": "11.10.3.66",
                "required_version": "==11.10.3.66"
            },
            {
                "key": "nvidia-cuda-cupti-cu11",
                "package_name": "nvidia-cuda-cupti-cu11",
                "installed_version": "11.7.101",
                "required_version": "==11.7.101"
            },
            {
                "key": "nvidia-cuda-nvrtc-cu11",
                "package_name": "nvidia-cuda-nvrtc-cu11",
                "installed_version": "11.7.99",
                "required_version": "==11.7.99"
            },
            {
                "key": "nvidia-cuda-runtime-cu11",
                "package_name": "nvidia-cuda-runtime-cu11",
                "installed_version": "11.7.99",
                "required_version": "==11.7.99"
            },
            {
                "key": "nvidia-cudnn-cu11",
                "package_name": "nvidia-cudnn-cu11",
                "installed_version": "8.5.0.96",
                "required_version": "==8.5.0.96"
            },
            {
                "key": "nvidia-cufft-cu11",
                "package_name": "nvidia-cufft-cu11",
                "installed_version": "10.9.0.58",
                "required_version": "==10.9.0.58"
            },
            {
                "key": "nvidia-curand-cu11",
                "package_name": "nvidia-curand-cu11",
                "installed_version": "10.2.10.91",
                "required_version": "==10.2.10.91"
            },
            {
                "key": "nvidia-cusolver-cu11",
                "package_name": "nvidia-cusolver-cu11",
                "installed_version": "11.4.0.1",
                "required_version": "==11.4.0.1"
            },
            {
                "key": "nvidia-cusparse-cu11",
                "package_name": "nvidia-cusparse-cu11",
                "installed_version": "11.7.4.91",
                "required_version": "==11.7.4.91"
            },
            {
                "key": "nvidia-nccl-cu11",
                "package_name": "nvidia-nccl-cu11",
                "installed_version": "2.14.3",
                "required_version": "==2.14.3"
            },
            {
                "key": "nvidia-nvtx-cu11",
                "package_name": "nvidia-nvtx-cu11",
                "installed_version": "11.7.91",
                "required_version": "==11.7.91"
            },
            {
                "key": "sympy",
                "package_name": "sympy",
                "installed_version": "1.12",
                "required_version": null
            },
            {
                "key": "triton",
                "package_name": "triton",
                "installed_version": "2.0.0",
                "required_version": "==2.0.0"
            },
            {
                "key": "typing-extensions",
                "package_name": "typing-extensions",
                "installed_version": "4.5.0",
                "required_version": null
            }
        ]
    },
    {
        "package": {
            "key": "torchvision",
            "package_name": "torchvision",
            "installed_version": "0.15.2"
        },
        "dependencies": [
            {
                "key": "numpy",
                "package_name": "numpy",
                "installed_version": "1.24.3",
                "required_version": null
            },
            {
                "key": "pillow",
                "package_name": "pillow",
                "installed_version": "9.5.0",
                "required_version": ">=5.3.0,!=8.3.*"
            },
            {
                "key": "requests",
                "package_name": "requests",
                "installed_version": "2.30.0",
                "required_version": null
            },
            {
                "key": "torch",
                "package_name": "torch",
                "installed_version": "2.0.1",
                "required_version": "==2.0.1"
            }
        ]
    },
    {
        "package": {
            "key": "tornado",
            "package_name": "tornado",
            "installed_version": "6.3.2"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "tqdm",
            "package_name": "tqdm",
            "installed_version": "4.65.0"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "traitlets",
            "package_name": "traitlets",
            "installed_version": "5.9.0"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "transformers",
            "package_name": "transformers",
            "installed_version": "4.29.2"
        },
        "dependencies": [
            {
                "key": "filelock",
                "package_name": "filelock",
                "installed_version": "3.12.0",
                "required_version": null
            },
            {
                "key": "huggingface-hub",
                "package_name": "huggingface-hub",
                "installed_version": "0.14.1",
                "required_version": ">=0.14.1,<1.0"
            },
            {
                "key": "numpy",
                "package_name": "numpy",
                "installed_version": "1.24.3",
                "required_version": ">=1.17"
            },
            {
                "key": "packaging",
                "package_name": "packaging",
                "installed_version": "23.1",
                "required_version": ">=20.0"
            },
            {
                "key": "pyyaml",
                "package_name": "pyyaml",
                "installed_version": "6.0",
                "required_version": ">=5.1"
            },
            {
                "key": "regex",
                "package_name": "regex",
                "installed_version": "2023.5.5",
                "required_version": "!=2019.12.17"
            },
            {
                "key": "requests",
                "package_name": "requests",
                "installed_version": "2.30.0",
                "required_version": null
            },
            {
                "key": "tokenizers",
                "package_name": "tokenizers",
                "installed_version": "0.13.3",
                "required_version": ">=0.11.1,<0.14,!=0.11.3"
            },
            {
                "key": "tqdm",
                "package_name": "tqdm",
                "installed_version": "4.65.0",
                "required_version": ">=4.27"
            }
        ]
    },
    {
        "package": {
            "key": "triton",
            "package_name": "triton",
            "installed_version": "2.0.0"
        },
        "dependencies": [
            {
                "key": "cmake",
                "package_name": "cmake",
                "installed_version": "3.26.3",
                "required_version": null
            },
            {
                "key": "filelock",
                "package_name": "filelock",
                "installed_version": "3.12.0",
                "required_version": null
            },
            {
                "key": "lit",
                "package_name": "lit",
                "installed_version": "16.0.3",
                "required_version": null
            },
            {
                "key": "torch",
                "package_name": "torch",
                "installed_version": "2.0.1",
                "required_version": null
            }
        ]
    },
    {
        "package": {
            "key": "trove-classifiers",
            "package_name": "trove-classifiers",
            "installed_version": "2023.5.2"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "twine",
            "package_name": "twine",
            "installed_version": "4.0.2"
        },
        "dependencies": [
            {
                "key": "importlib-metadata",
                "package_name": "importlib-metadata",
                "installed_version": "4.13.0",
                "required_version": ">=3.6"
            },
            {
                "key": "keyring",
                "package_name": "keyring",
                "installed_version": "23.13.1",
                "required_version": ">=15.1"
            },
            {
                "key": "pkginfo",
                "package_name": "pkginfo",
                "installed_version": "1.9.6",
                "required_version": ">=1.8.1"
            },
            {
                "key": "readme-renderer",
                "package_name": "readme-renderer",
                "installed_version": "37.3",
                "required_version": ">=35.0"
            },
            {
                "key": "requests",
                "package_name": "requests",
                "installed_version": "2.30.0",
                "required_version": ">=2.20"
            },
            {
                "key": "requests-toolbelt",
                "package_name": "requests-toolbelt",
                "installed_version": "0.10.1",
                "required_version": ">=0.8.0,!=0.9.0"
            },
            {
                "key": "rfc3986",
                "package_name": "rfc3986",
                "installed_version": "2.0.0",
                "required_version": ">=1.4.0"
            },
            {
                "key": "rich",
                "package_name": "rich",
                "installed_version": "13.3.5",
                "required_version": ">=12.0.0"
            },
            {
                "key": "urllib3",
                "package_name": "urllib3",
                "installed_version": "1.26.15",
                "required_version": ">=1.26.0"
            }
        ]
    },
    {
        "package": {
            "key": "typer",
            "package_name": "typer",
            "installed_version": "0.7.0"
        },
        "dependencies": [
            {
                "key": "click",
                "package_name": "click",
                "installed_version": "8.1.3",
                "required_version": ">=7.1.1,<9.0.0"
            }
        ]
    },
    {
        "package": {
            "key": "typing-extensions",
            "package_name": "typing-extensions",
            "installed_version": "4.5.0"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "tzdata",
            "package_name": "tzdata",
            "installed_version": "2023.3"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "uc-micro-py",
            "package_name": "uc-micro-py",
            "installed_version": "1.0.2"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "unidecode",
            "package_name": "Unidecode",
            "installed_version": "1.3.6"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "uri-template",
            "package_name": "uri-template",
            "installed_version": "1.2.0"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "urllib3",
            "package_name": "urllib3",
            "installed_version": "1.26.15"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "uvicorn",
            "package_name": "uvicorn",
            "installed_version": "0.22.0"
        },
        "dependencies": [
            {
                "key": "click",
                "package_name": "click",
                "installed_version": "8.1.3",
                "required_version": ">=7.0"
            },
            {
                "key": "h11",
                "package_name": "h11",
                "installed_version": "0.14.0",
                "required_version": ">=0.8"
            }
        ]
    },
    {
        "package": {
            "key": "virtualenv",
            "package_name": "virtualenv",
            "installed_version": "20.21.1"
        },
        "dependencies": [
            {
                "key": "distlib",
                "package_name": "distlib",
                "installed_version": "0.3.6",
                "required_version": ">=0.3.6,<1"
            },
            {
                "key": "filelock",
                "package_name": "filelock",
                "installed_version": "3.12.0",
                "required_version": ">=3.4.1,<4"
            },
            {
                "key": "platformdirs",
                "package_name": "platformdirs",
                "installed_version": "2.6.2",
                "required_version": ">=2.4,<4"
            }
        ]
    },
    {
        "package": {
            "key": "wasabi",
            "package_name": "wasabi",
            "installed_version": "0.10.1"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "wcwidth",
            "package_name": "wcwidth",
            "installed_version": "0.2.6"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "webcolors",
            "package_name": "webcolors",
            "installed_version": "1.13"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "webencodings",
            "package_name": "webencodings",
            "installed_version": "0.5.1"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "websocket-client",
            "package_name": "websocket-client",
            "installed_version": "1.5.1"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "websockets",
            "package_name": "websockets",
            "installed_version": "11.0.3"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "wheel",
            "package_name": "wheel",
            "installed_version": "0.40.0"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "widgetsnbextension",
            "package_name": "widgetsnbextension",
            "installed_version": "4.0.7"
        },
        "dependencies": []
    },
    {
        "package": {
            "key": "yarl",
            "package_name": "yarl",
            "installed_version": "1.9.2"
        },
        "dependencies": [
            {
                "key": "idna",
                "package_name": "idna",
                "installed_version": "3.4",
                "required_version": ">=2.0"
            },
            {
                "key": "multidict",
                "package_name": "multidict",
                "installed_version": "6.0.4",
                "required_version": ">=4.0"
            }
        ]
    },
    {
        "package": {
            "key": "zipp",
            "package_name": "zipp",
            "installed_version": "3.15.0"
        },
        "dependencies": []
    }
]
